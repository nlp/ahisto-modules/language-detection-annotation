# AHISTO Language Detection

 [[_TOC_]] 
 
This repository contains a random sample of 200 relevant pages from the AHISTO
dataset. The sample has been produced using the [`sample_relevant_pages.py`][1]
script from [`xnovot32/ahisto-tesseract`][2].

The repository consists of the following parts:

- [`annotations/*.md`][5]: Output markdown files with language annotations
  using an easy-to-read-and-write text format. The format of the markdown
  files is described [below](#format).

- [`templates/*.hocr`][6]: Input HOCR XML files with OCR texts. These are used as a
  template to reconstruct structured OCR texts out of the annotated markdown
  files.

- [`scripts/*.py`][7]: Conversion functions between HOCR XML and markdown.

- [`.gitlab-ci.yml`][8]: Continuous integration, which is executed at each
  commit and validates that structured OCR texts can be extracted out of the
  annotated markdown files. Uses the conversion functions from `scripts/*.py`.

## Format of language annotations

<a name="format"></a>

The markdown files with language annotations have the following format, which uses
the [`inline_code_attributes`][3] and [`hard_line_breaks`][10] syntax extensions
of Pandoc.

``` markdown

[![page image][image]][portal]

 [image]:  https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/XXX/YYY.jpg
 [portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=XXX&page=YYY

***

`16`{.unchecked lang=deu}   
`Also ir uns geschriben habt etc. Lorem ipsum`{.unchecked lang=deu}   
`dolor sit amet, consectetuer adipiscing elit. Žižka, šiška.`{.unchecked lang=lat}   
`be68ASDG38ca8DFG686e5c9DFG0689bf2ab`{.unchecked lang=lat}   

```

Everything above `***` is ignored during the conversion.
You can put your own editorial notes there if you wish.

Every non-blank line below `***` corresponds to a line of OCR text.
Spans of text in a single language are marked up as `` `text`{ATTRIBUTES} ``,
where *ATTRIBUTES* is a space-delimited sequence of one of the following:

- `lang=LANGUAGE`, where *LANGUAGE* is [an ISO 639‑2 code][4],
- `.unchecked` if a line has not yet been checked by a human annotator,
- `.error` if a span of text corresponds to a non-text (e.g. a graphic element
  or a speckle) that has been incorrectly recognized as text.

Not every span of text needs to have a language assigned. For example, non-text
(`.error`) or language-agnostic information that are not embedded in a paragraph
(e.g. page numbers) may not have a language associated with them.

Here is how the above markdown file may look after an annotator is done with it:

``` markdown

[![page image][image]][portal]

 [image]:  https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/XXX/YYY.jpg
 [portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=XXX&page=YYY

Note: Most of the page is taken up by an image, which Tesseract incorrectly
recognized as text. Aargh!

***

`16`   
`Also ir uns geschriben habt etc.`{lang=deu} `Lorem ipsum`{lang=lat}   
`dolor sit amet, consectetuer adipiscing elit.`{lang=lat} `Žižka, šiška.`{lang=ces}   
`be68ASDG38ca8DFG686e5c9DFG0689bf2ab`{.error}   

```

## How do I annotate?

You can pick any markdown file from the [`annotations/`][5] directory and edit it in
GitLab's text editor by clicking the *Edit* button in your web browser.
Alternatively, you can clone the Git repository and use your favorite text
editor Vim.

As a first action, rename the file with some extra word meaning it is being worked on, 
e.g.  `XXX-YYY rozprac.md` or `XXX-YYY your_name.md`. In Gitlab, renaming is done in 
the *Edit* window – just alter the name at the top and press *Commit changes*.
After this change, edit the content of the renamed file.

Removing the `.unchecked` attribute from all lines by hand can be a chore. In
GitLab's text editor, you can use the search-and-replace function by hitting
Ctrl+H (or Ctrl+F and click on `>` in Firefox) and replacing all occurrences 
of `{.unchecked ` with `{ `. The *Find* operation here can also use regular
expressions, so e.g. all erroneous annotations can be easily changed with 
a search for `\{lang=...\}` and replaced with the correct one.

When you are done annotating a file, rename it from `XXX-YYY.md` to `XXX-YYY
hotovo.md`, so that it is clear to everyone that this file has been finished.

After you have committed your changes, continuous integration will validate that
the markdown file can be converted to HOCR XML after your changes. This will be
indicated by an icon next to the [commit][9]: ✔️ for success and ❌ for failure.
Click the icon to inspect the logs, which will include useful information such
as:

- Which lines caused the validation to fail (if any).
- What portion of the dataset has already been annotated.
- What is the proportion of languages in the dataset.

 [1]: https://gitlab.fi.muni.cz/xnovot32/ahisto-tesseract/-/blob/master/scripts/sample_relevant_pages.py
 [2]: https://gitlab.fi.muni.cz/xnovot32/ahisto-tesseract
 [3]: https://pandoc.org/MANUAL.html#extension-inline_code_attributes
 [4]: https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
 [5]: annotations/
 [6]: templates/
 [7]: scripts/
 [8]: .gitlab-ci.yml
 [9]: https://gitlab.fi.muni.cz/nlp/ahisto-language-detection/-/commits/
 [10]: https://pandoc.org/MANUAL.html#extension-hard_line_breaks
