[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/224.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=224

***

`212` `Urfunden zum Jahre 1424.`{lang=deu}   
`Or`   
`20`   
`25`   
`30`   
`85`   
`das ir den Czittawern helffet, dasselbe Newehaws wider zu bauen,`{lang=deu}   
`das zu beseczen, zu  behalden, und auch mit aller euer macht das`{lang=deu}   
`lande zu weren, das euch die fiende furbasz kein nehende!) mer`{lang=deu}   
`angewunnen und zu grossern scheden bringen. Als wir den`{lang=deu}   
`namhafftigen Bernharten von Dowischuez unsern lieben getruen`{lang=deu}   
`mit unser meynung dorumb zu euch senden, dem wollet dorynn`{lang=deu}   
`als uns selber genzlich glauben, und euch auch also beweisen,`{lang=deu}   
`das wir euch dorumb geloben mugen. Das wollen wir euch in`{lang=deu}   
`allem gute gnediclich gedenken. Geben zu Crokaw am sampsztag`{lang=deu}   
`vor dem suntag reminiscere, unser riche des Ungarischen ete. in`{lang=deu}   
`dem 37., des Romischen in dem 14. und des Behemischen im`{lang=deu}   
`virden jaren.`{lang=deu}   
`Per dominum Janconem de Chotiemiez`{lang=lat}   
`Michael praepositus Boleslaviensis.`{lang=lat}   
`1424. März 24.`{lang=deu}   
`Casper von Nosticz est vocatus a Nickil Wusch pro homi-`{lang=lat}   
`cidio in Bartil Wusch commisso. |`{lang=lat}   
`Aus dem Górlier liber vocacionum 1594—1432 im Górlier Rats:`{lang=deu}   
`archive.`{lang=deu}   
`[1424]. April 18.`{lang=deu}   
`Verzeichnis der am dinste noch Palmen kein der Zittaw von`{lang=lat}   
`Gürlilz geschickten Wepner.`{lang=lat}   
`In den Beilagen zu den Góorliker Ratsrechnungen 1428 (P) uno 1440 ff.`{lang=deu}   
`im Górlier Ratsarchive.`{lang=deu}   
`1434. Mai 13.`{lang=deu}   
`Caspar Nosticz est vocatus a OCristoff Wusch pro vulnere`{lang=lat}   
`carnis. — Caspar, von Notenhofe est vocatus a Hanno Wendeler`{lang=lat}   
`pro mutilacione et ferlich uffene fleischwunde in eo commisso.`{lang=lat}   
`Aus dem Gôrliter liber vocacionum 1394—1432 im Górlier Rats:`{lang=deu}   
`archive.`{lang=deu}   
`1424. Juli 7.`{lang=deu}   
`Caspar Notenhofe zu Arnsdorf juravit met 7 pro mutilatione`{lang=lat}   
`et juravit 1 pro volnere campher in Hannus Wedeler commisso`{lang=lat}   
`et resignavit circa omnia bona summo jure pro pace tenenda.`{lang=lat}   
`Aus dem Sörliger liber vocacionum 1594—1452 im Górlier Rats:`{lang=deu}   
`archive.`{lang=deu}   
`1) fein euch nabeliegendes (Schloß).`{lang=deu}   
