[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/60.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=60

***

`48` ` Urfunden sum Jahre 1421.`{lang=deu}   
`[1421]. Mai 30. Tollenstein.!)`{lang=deu}   
`Hinko Berka von der Duba am die Stadt [Zittau]: Leitmeritz`{lang=deu}   
`ist den Hussiten übergeben. Die Hussiten sind zurück nach`{lang=deu}   
`Hosko (Gastdorf) gerückt und bedrohen Leipa. Bitte um Hilfe.`{lang=deu}   
`5 Yus Scultet. annal. IL 49b, 50a. Gedruckt bei Paladďy, Urfundliche`{lang=deu}   
`Beiträge I. S. 106.`{lang=deu}   
`Meinen dinst zuvor, liben bürger und ratmanne, 1ch thue`{lang=deu}   
`euch wissen, wie das Luthemeritz gegeben ist und sie zurücke`{lang=deu}   
`gerückt sein gen der Hoszko?): do bitt ich euch und ermane`{lang=deu}   
`10 euch von meins herrn des koniges wegen, noch heutest — wenn`{lang=deu}   
`ich euch selber vormals müntlich irmant habe, das man der von`{lang=deu}   
`Luthomeritz gerett hette, das ist nicht geschehen — so bitt ich`{lang=deu}   
`^ euch noch durch meines dinstes wile, das ir mir wellet ausz-`{lang=deu}   
`richten 200 schützen geryten oder zu fuss, dy do werhaftig weren`{lang=deu}   
`15 und zu were tüchten, hin gen Lippen yon stund an, das ich die`{lang=deu}   
`stat und slos meinem hern dem konige zu sein eren behalden`{lang=deu}   
`mochte ete. Geben zu Tollenstein am freitage `{lang=deu} `post octavas cor-`{lang=lat}   
`poris Christi.`{lang=lat}   
`Hincke Bercke Hlawatzs von der Duba, herre zur Lypen etc.`{lang=deu}   
`20 Gur Datierung: Die erwdhnte Übergabe der Stadt Seitmeri& an die Huffiten`{lang=deu}   
`gefdyaly am 29. Mat 1421.`{lang=deu}   
`[1421]. Mai 30. Zittau.`{lang=deu}   
`Thamme von Gersdorff und der Rat zu Zittau überschicken den`{lang=deu}   
`| Górlitzern eine Abschrift eines Briefes des Hinko Berka von`{lang=deu}   
`25 der Dube und bitlen, demfelben die erbetene Hilfe zu leisten.`{lang=deu}   
`| Aus Scultet, annal, II, 50a,`{lang=deu}   
`Wir senden euch hiermit eine abeschrift des edlen Hlawatzen`{lang=deu}   
`briffes von der Lippen, den er uns heute umb 923 st. gesant hat,`{lang=deu}   
`ylende bey eyme reitenden boten ete. Deuchte uns als vorne,`{lang=deu}   
`30 das unserm hern dem voite, andern landen und steten, den wir`{lang=deu}   
`dis ding auch geschriben haben, wolgewilt, das her Hlawatcz`{lang=deu}   
`mit solcher hilffe unserem gnedigen herrn dem kónige zu eren etc.`{lang=deu}   
`nicht zu verlassen were etc., nochdem als das im von landen und`{lang=deu}   
`...1) In der Dorlage fteht Stellenstein oder Stollenstein. Da eine derartige`{lang=deu}   
`Ortichaft in XToroběhmen nicht eriftirt, it wohl Collenftein_3u fchreiben; denn an`{lang=deu}   
`Stolinky (Drum, f. Sommer, bas Kar, Böhmen I. S. 329) farm wohl nicht gedacht`{lang=deu}   
`werden.`{lang=deu}   
`2) Deral. die Urkunde vom 12. Juni 1422, nach der Hosko eine andere Benennung +`{lang=deu}   
`für Gaftdorf ift. . -`{lang=deu}   
`ен`{.error}   
