[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/793/463.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=793&page=463

***

`Е. Ш. Registra zdpisuw r. 1454. 463`{lang=ces}   
`a tudíž sedční, Waclawowi otci swrchupsaného Sigmunda pacholika, a to w Sti-`{lang=ces}   
`boteti (sic) jemu i jeho dědicuom, jakož týž list site swèdéi, jehož dat. 1. 1431,`{lang=ces}   
`w středu před božím tělem.`{lang=ces}   
`581. Okázán list německý od kněze Bedřicha, od opata Dětřicha a kon-`{lang=ces}   
`venta klastera Sedleckého, prodawaci wes a dwuor Lužow se wším příslušenstwim,`{lang=ces}   
`Jakubowi Wilmanowi mésténinu u Hory i jeho dédicuom a budaucim, jmenowité`{lang=ces}   
`20 ff gr. platu; pakliby tu nepostihli 20 ff gr., ale na wšem zboZi jich za 200 [В`{lang=ces}   
`gr. k wayplatě, jakož týž šíře swědčí.`{lang=ces}   
`582. Ondřej z Drachowa okázal nám list s majestátem krále Waclawa,`{lang=ces}   
`kterýž jeho otci Bohušowi z Drachowa swědči, na podací oltáře a kaplana matky`{lang=ces}   
`boii ke cti sc. w kostele Pražském, i na to, což ktomu oltáři platu přísluší, jakož`{lang=ces}   
`týž list šíře: swědčí.`{lang=ces}   
`583. Simonek z Jiloweho okäzal list kněze Františka opata a konventu`{lang=ces}   
`klástera Ostrowského, swédéici na 28 IM gr. dluhu Jakubowi a Jostowi bratřím`{lang=ces}   
`z Jiloweho.`{lang=ces}   
`K tomu se před námi přiznala Katruše z Jilowého, sestra Jakubowa z Jilowého, że`{lang=ces}   
`ten list Šimonkowi dáwá swobodně.`{lang=ces}   
`584. Racek z Risemberka okazal nám list se tfemi peëetmi od Zdeńka`{lang=ces}   
`z Dritky purkrabie llorsowskóho, Zdeńka z Drštky řečeného Kolwin, Wiléma ze`{lang=ces}   
`Zdára, jistcuow i rukojmi, swédéici na dluh 500 MM gr. Janowi z Wildemfelsu a`{lang=ces}   
`Janowi ze Zdiáru oc. a jim w tom sstüpili w Stankowách a w Holuéowách wieho`{lang=ces}   
`zboží oc.`{lang=ces}   
`585. Okázán nám list jmónem Tomka z Knienic od knóze Petra opata a`{lang=ces}   
`konventa klástera Wilemského, kteryż swódói Benartowi z Okrdhlice a jeho dódi-`{lang=ces}   
`cuom i budaucim na wsi Okrdhlice a Chlistow, pod purkrecht piewedeno na téhož`{lang=ces}   
`Bernarta i jeho dědice i budauci od Śtefana oc. s wyminkami w tom listu polożenymi.`{lang=ces}   
`586. Pawel Křižan z Hrádku swym a Chmelikowym jménem оКаха] dwa`{lang=ces}   
`listy: jeden Jana opata a wieho konventa klastera Hradistskóho, swedóici Jakubowi`{lang=ces}   
`řečenému Chmelik z Ujezda a Hrzkowi bratru jeho a jich budaucím, na prawé dě-`{lang=ces}   
`dictwi a swobodny purkrecht k wěčnosti, na wes Bušow, sobě panstwi pozuosta-`{lang=ces}   
`wujíc, s wýminkami pod úrok, jakž tyż list šíře swédéi; a druhy list s menší: pečeti`{lang=ces}   
`krále Sigmunda, téhož trhu a prodaje i kaupeni ku purkrechtu potwrzujici.`{lang=ces}   
`587. Mikulas z Lobkowic okazal tii listy: jeden swédéi s krále Wáclawo-`{lang=ces}   
`wym majestátem Mikulášowi z Ujezda na duom u Hory Glecorów s wysokau wśżi,`{lang=ces}   
`jakoż ten list dale swódói, i na druhý duom, jenž leží na koňském trhu u Hory.`{lang=ces}   
