[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/84.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=84

***

`(2`   
`10`   
`15`   
`20`   
`30`   
`Urfunden zum Jahre 1421/22.`{lang=deu}   
`tilgen unde dy fursten uffnehmen etc. Gegeben am montage`{lang=deu}   
`vor Hedwigis zu felde vor Mittenwalde.`{lang=deu}   
`Iterum postscriptum.`{lang=lat} ` Wir müssen keuffen !/, byers vor`{lang=deu}   
`l mark, vor 4!/; schilmg unde auch teuer und neher') und`{lang=deu}   
`mogen mit 4 fudir biers den tag nicht zukommen unde ynand?)`{lang=deu}   
`musen wir haben ?/, virtel, auch hat man ir */, eynen tag aus-`{lang=deu}   
`getruncken. Noch mussen wir horen, Swobe sey wedir uffge-`{lang=deu}   
`standen. Ouch, liben herren, haben wir zu speisen 20 menschen`{lang=deu}   
`unde 200 adir me, das last euch auch zu hertzen gan.`{lang=deu}   
`Herman Schultes?) Hannus Pletzil unde Heinrich Ebirhard,`{lang=deu}   
`der sliff, den irbern weysen burgermeister unde ratmann der`{lang=deu}   
`stat Gorlicz.`{lang=deu}   
`Griinhagen datiert das Hauptfchreiben vom 10., Dalady vom 12. Oftober,`{lang=deu}   
`die Derfdytedenheit Fommt daher, bai der Tag des heiligen Burchard fowohI`{lang=deu}   
`auf den N. als auf den 14. Oftober füllt (bei Dalady ift wohl irrtümlich`{lang=deu}   
`der 12. für den 13. Oftober gefebt). Die Datiernnaen im erften `{lang=deu} `postseriptum`{lang=lat}   
`erweifen, daß das richtige Datum der 10. Oftober ift.`{lang=deu}   
`„1421. Dezember 31 bis 1422 August 5.“`{lang=deu}   
`Unter den Propinationes ( Verehrungen in Getränken) der Stadt`{lang=deu}   
`Nürnberg vor und bei dem Reichstage daselbst (vom Juli bis`{lang=deu}   
`September 1422) findet sich: „Propinavimus den von Pawdisz-`{lang=deu}   
`heim und den von Görlitz und den von der Syttaw 10 gr.;`{lang=deu}   
`summa 1 lb. 9 sh. 2 hllr.`{lang=deu}   
`Aus Dentfhe Reidhstagsaften VIII. S. 230, 5 ff.`{lang=deu}   
`Don Górli war nur der Stadtdiener Baffe mit in Miirnberg; der Sübrer der`{lang=deu}   
`Gejandtfchaft war Punzel von Bauten, der gegen Ende Juli 1422 wegritt uno`{lang=deu}   
`in der Woche nach dem 12. September 1422 zurückfam, f. unten Nr. Ś. ss, 29`{lang=deu}   
`und S. 92, 14`{lang=deu}   
`1421/1422 Oktober — Oktober.`{lang=deu}   
`Einnahme und Ausgabe der Stadt Görlitz.`{lang=deu}   
`Hr. V. 64a —70b, 138 а.`{lang=deu}   
`1) Die Einnahmen des Jahres betrugen 2193 sch. minus`{lang=deu}   
`6 gr., die Summe der beiden Geschosse 1331 sch. 13 gr. und zwar`{lang=deu}   
`[Bl. 67a] Von deme geschosse noch cireumdederunt [1422. Febr. 8]`{lang=deu}   
`1) S Fanm richtig, „auch theuerer und mehr“?`{lang=deu}   
`2) Sit mir unflar.`{lang=deu}   
`3) Zlicht wie Scultet. fdreibt Schultz.`{lang=deu} ` |`{.error}   
`©`{lang=deu} ` „qui dormiebat“;`{lang=lat} ` einen Beinamen „der Slif“ mit Griinhagen und Palacy`{lang=deu}   
`‚möchte th nicht annehmen, da in den ziemlich zahlreichen Urkunden, in denen 6. Ebir-`{lang=deu}   
`hard vorfommt, fich ein folcher nicht findet.`{lang=deu}   
