[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/114.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=114

***

`102 Urkunden zum Jahre`{lang=deu} `122.`   
`ot`   
`10`   
`20`   
`25`   
`30`   
`35`   
`nach Böhmen ziehen wolle. Die Breslauer möchten im Vereine`{lang=deu}   
`mit den Fürsten diesen Durchzug hindern helfen.`{lang=deu}   
`Aus Stadtarchiv Breslau Correfp.`{lang=deu}   
`Überfchrift, Datum und Adreffe fehlen.`{lang=deu}   
`1422. Januar 9. Glogau.`{lang=deu}   
`Herzog Heinrich der Altere von Glogau giebt den drei Ge-`{lang=deu}   
`brüdern Hans, Heinrich und Christoph von Gersdorff frei Ge-`{lang=deu}   
`leite im Görlitzer Lande bis zum 25. Februar.`{lang=deu}   
`| Aus Scultet. annal. IL. BI. 58b unter 9. J. 1422.`{lang=deu}   
`Wir Heinrich der eldir von gotis gn. herzog in Schlesien`{lang=deu}   
`und hirre zu grosin Glogaw etc. tuwen kund allin und itzlichin,`{lang=deu}   
`sunderlich euch besundern liben heuptlauten, richtern, burger-`{lang=deu}   
`meistir und ratman der stat Görlitz, das wir gebin und gegebin |`{lang=deu}   
`habin eyn recht cristinlich geleyte Hanus Gebeltzk zum Loden`{lang=deu}   
`gesessen und Heynrich Gerisstorf und Jorgen Gerisstorf, ge-`{lang=deu}   
`brüdern, zwischen hy und fasznacht vor alle den, die durch`{lang=deu}   
`unsern willen thun und lassin wellin, das sy sicher abe und zu`{lang=deu}   
`zihen und rostin mogen im lande zu Gorlitz, wo sie zu schaffin`{lang=deu}   
`haben. Gegeben zu Glogaw am fritage nach epiphania domini`{lang=deu}   
`anno 1422. |`{lang=deu}   
`1422. Januar 22. Sprottau.`{lang=deu}   
`Herzog Heinrich der Ältere von Glogau entschuldigt sich, dass`{lang=deu}   
`er für jetzt Geschäfte halber nicht [in die Oberlausitz/ kommen`{lang=deu}   
`könne.`{lang=deu}   
`Aus Scultet annal. IL BL 59b unter d. 3. 1422.`{lang=deu}   
`Also ir uns geschriben habt etc. und lossin euch wissen,`{lang=deu}   
`das wir itzund vor unsern merglichen gescheften nicht kommen`{lang=deu}   
`kónnen; so wir alhier schierste mógin, wellen wir gerne zu euch`{lang=deu}   
`kommen und wellin von unseres gn. herren mit landen und mit`{lang=deu}   
`steten mit euch des landis besten helfin handiln und schicken`{lang=deu}   
`des bestin desz wir kónnen odir mógin. Und bethin euch, das`{lang=deu}   
`ir uns hierinne nicht verdenkin wellit ete. Gegebin zur Sprottaw`{lang=deu}   
`am tage Vincentii martyris [anno 1422].`{lang=deu}   
`Heinrich der elder v. g. gn. herzog und hirre zu Glogaw`{lang=deu}   
`etc. an dies land und stete.`{lang=deu}   
`Da bei Scultet. die Bezeichnung am Rande (mit roter Tinte) „sine anno“`{lang=deu}   
`fehlt, fo fand er jedenfalls die Urfunde mit der ^ahressahl 122 vetfehen.`{lang=deu}   
