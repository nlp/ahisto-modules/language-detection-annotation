[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/338.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=338

***

`326` `Urfunden zum Jahre 1426.`{lang=deu}   
`20`   
`ty`   
`c`   
`30`   
`1426. Mai 28. Görlitz.`{lang=deu}   
`Die Görlitzer schreiben an Wenzlaus von Polenz, da die Ketzer`{lang=deu}   
`sich wieder von ihnen abgewendet hiitten, so bediirften sie seiner`{lang=deu}   
`Söldner nicht mehr. Bei Bedarf würden sie sich wieder an`{lang=deu}   
`ihn wenden. |`{lang=deu}   
`us Milichicje Bibliothef in Górlik mspt. fol. 217 no. 168 anf der`{lang=deu}   
`Rückfeite, Konzept mit vielen Korrekturen. Gedruckt bei Palady,`{lang=deu}   
`Urfundliche Beiträge I S. 456; Reaeft bet Worbs, Invent. diplom.`{lang=deu}   
`Lusat. infer, S. 240.`{lang=deu}   
`Unsen fruntlichen dinst zuvor, lieber Wenczlaw! Als wir`{lang=deu}   
`mit uch gereth habin und ir weder mit uns von uwren frunde`{lang=deu}   
`und gesellin wegin, uns die zu furen eto., hoffen wir, das wir der`{lang=deu}   
`zu desim mole nicht notdurft sein, wenne die keczer von uns`{lang=deu}   
`weder wendig wurdin sein, und dangkin uch sulchir fruntschaft`{lang=deu}   
`und uwre muhe, die ir umb unszir willin gehabit hat, und wollin`{lang=deu}   
`is umbe uch sunderlich und mit willen verdinen. Wurde is uns`{lang=deu}   
`aber furder not geschen, so wolle wir is uch zeitlich genug`{lang=deu}   
`lassin wissen. Datum Gorlicz am dinstage noch trinitatis`{lang=deu}   
`anno ete. 26.`{lang=deu}   
`Zu vergl. oben S. 283, 7 f.; 284, 31 ff.`{lang=deu}   
`1426. [Ende Mail.`{lang=deu}   
`Нет Nicklaus Gessener dedit 1 sch., das her nicht hat durft`{lang=deu}   
`in die herfart methe sendin kein der Dese und Drausendorff.`{lang=deu}   
`Mus Górl. Жк. УГ BL 32b.`{lang=deu}   
`Die Datierung nach der Urkunde 1426. Mai 26 ff.`{lang=deu}   
`[1426. Ende Mai oder im Juni].`{lang=deu}   
`Die Höhe des Lösegeldes und die Bürgen für die in Böhmisch-`{lang=deu}   
`Leipa gefangenen Oberlausitæer.`{lang=deu}   
`Uus Milidfdhe Biblioihef mspt. fol. 230 S. 76. Abfdrift in Sculteti`{lang=deu}   
`annal. IE. 231. 83b f., danad) georueËt bei Palady, Urfunol, Betträge II`{lang=deu}   
`S. 534 f. Ubfdrift in Klof diplomatar. II 5. 26 f.`{lang=deu}   
`Ttem nota die gefangen, dye zur Leyppen gefangen sitezen,`{lang=deu}   
`die haben sich geschaczt kein den edlen herrn Hlawacz Birke`{lang=deu}   
`von der Dubin umbe 200 sch. gr. Und die selben gefangen`{lang=deu}   
`sint von der Zittaw, Budissin, Gorlicz, Luban und Kamencz, unde`{lang=deu}   
`dye selbin 200 sch. gr. sullen geben und bezalen von datum`{lang=deu}   
`diz brifs in 15 wochin, 100 sch. gr. in achthalben wochin unde`{lang=deu}   
`100 sch. gr. dornach abir in achthalben woche. Und des sullen`{lang=deu}   
`sye dem edeln herrn Hlawaczen eyn brieff machin, im und seynen`{lang=deu}   
