[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/89.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=89

***

`30`   
`35`   
`Urkunden zum Jahre 1422. `{lang=deu} `77`   
`den keczern, als wir mit macht kein der Zitaw ziehen musten,`{lang=deu}   
`12 gr. — Einen boten zu den marggrofen ken Meissen durch`{lang=deu}   
`eines zoges wille keyn Dressdan 8 gr. — Eyme spheer kein der`{lang=deu}   
`Dhome!) in das land durch heymelicher sache wille 10 gr. —`{lang=deu}   
`Herman Schultes, Heinrich Ebirhart worden geschicket kein.`{lang=deu}   
`Hoerswerde, do ging die reise abe, doroff ist gegangen 10 gr. —`{lang=deu}   
`Honoratio Lubanorum 6 gr. — Einen boten kein Glogaw zu`{lang=deu}   
`Heinrich deme eldern durch geldes wile, das im unde seinem`{lang=deu}   
`bruder herzoge Rumpolde gelegen ward, 8 gr. — [Bl 994]`{lang=deu}   
`. Dornoch quam herzoge der elder zu uns, der wart geert mit`{lang=deu}   
`wyne etc. 25 gr. — Do quomen land unde stete zu im, die`{lang=deu}   
`worden geert, per omnia, 18 gr. — Obir das wart betrunken of`{lang=deu}   
`deme rothuze 10 gr. — `{lang=deu} `Nuncium ad Jone Wartenberg cum`{lang=lat}   
`littera ducis`{lang=lat} ` 5 gr. — Item eynen boten abir kein der Swydenicz`{lang=deu}   
`mit des herzogen brifen 10 gr. — Einen wagen kein der Zitaw`{lang=deu}   
`mit vir pherden deme herzogen 16 gr. — Item einen boten ein`{lang=deu}   
`ritenden kein deme Sagan mit herzoge Heinrichs brife zu her-`{lang=deu}   
` zoge Hannus 8 gr. — ltem als die unseren vormols kein deme`{lang=deu}   
`Forste gereten unde drungen den Bresenern zweie pherde abe,`{lang=deu}   
`vor die pherde unde vil anderleie reiten unde grose mühe gab`{lang=deu}   
`man den schuczeń und dinern zu vortrinken 51% sch. 6 gr.`{lang=deu}   
`[BL 99%] Tnvocavit [März 1]:`{lang=deu}   
`Worden die unsern reitende unde off wagen unde fuss-`{lang=deu}   
`genger usgerichtet kein der Zitaw kein den ketzern unde`{lang=deu}   
`an das gebirge, doruff gegangen 1 sch. gr. — Lande unde stete,`{lang=deu}   
`als sie das dritte mol zu deme konige for sich zogen, worden`{lang=deu}   
`geert mit wyne unde bire 18 gr. — Einen ritenden boten zu`{lang=deu}   
`den unsern kein der Zitaw etc. 6 gr. — Dornoch quam der`{lang=deu}   
`herzog us der herfart von der Zitaw wider zu uns, wart geert`{lang=deu}   
`18 gr. — Also quomen die unsern alle mitenander us der herfart`{lang=deu}   
`her heym, den wart eyn essen gemacht 2!/& sch. — Her Cristoff`{lang=deu}   
`quam her unde reit mit Hannus Ulrsdorff zu unserm herren`{lang=deu}   
`deme konige, wart geert etc. 10 gr. — In der zeit trunken der`{lang=deu}   
`burgermeister, schepphen unde rothmannen mit den eldisten eyn`{lang=deu}   
`halb fuder bires mitenander off deme rothus durch vil not-`{lang=deu}   
`geschefte wille 11/2 sch. unde eyn firtel weissyn. — Item wort`{lang=deu}   
`herzoge Heinrich [geert] mit allen den unsern noch der herfart,`{lang=deu}   
`1) Es tft Дайте пей von Sudan gemeint.`{lang=deu}   
