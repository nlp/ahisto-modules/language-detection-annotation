[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/792/157.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=792&page=157

***

`C. L. Stare zápisy rodu Siernberskiho 1432. 157`{.unchecked lang=lat}   
`domie pečel statečných a slowütnych Wiléma z Postupic odjinud s Hrádku a Be-`{.unchecked lang=eng}   
`nese z Mokrowuüs ptiwèseny jsû wedlé mne k témuž listu. Jenž dán w neděli: In-`{.unchecked lang=eng}   
`vocavit, léta od narozenie syna bozieho tisicého étyrstého tiidcatého prwnieho.`{.unchecked lang=eng}   
`20.`{.unchecked lang=lat}   
`Ales Holicky ze Sternberka bratijm ze Swogsjna zapisuge na zboZj swém 400 kop`{.unchecked lang=eng}   
`gr. dluhu wénného za pannau Annau ze Sternberka.`{.unchecked lang=eng}   
`Na Kiiwokläte, 14 Mai 1432.`{.unchecked lang=eng}   
`Já Aleš ze Šternberka, odjinud z Holic, seděním na Hrádku Křiwoklátu,`{.unchecked lang=ces}   
`známo činím tiemto listem obecně wšem, kdož jej uzřie a čísti nebo čtúce slyšeti`{.unchecked lang=ces}   
`budů: že jsem dlužen jistého a sprawedliwého dluhu 400 kop gr. dobrých střie-`{.unchecked lang=ces}   
`brnych rázu Pražského a měny země České wěna prawého, kteréž jsem wónowal`{.unchecked lang=ces}   
`po urozené panně, panně Anně sestře, dceři někdy urozeného pána, pana Jaro-`{.unchecked lang=ces}   
`slawa ze Šternberka odjinud z Weselé, strýce mého milého, urozeným Petrowi,`{.unchecked lang=ces}   
`Jankowi a Waňkowi bratřím wlastním ze Swojšína, i tomu každému, kdožby tento`{.unchecked lang=ces}   
`list jmel s dobru jich wolí. А ty jisté penieze, 400 kop gr. swrchupsanych, jà`{.unchecked lang=ces}   
`Ales napredpsany jmám a slibuji tiemto listem mu dobrů čistů wérü krestanskuü,`{.unchecked lang=ces}   
`bcze wsi zlć lsti, wéricim mym nadepsanym oddati a odkázati na Třešti, na tom`{.unchecked lang=ces}   
`na wšem zboží k Třešu přislušejícím, jestli že mi to zbožie již feéené w té miete`{.unchecked lang=ces}   
`w mé prawé plné swobodné drženie přijde, tak, aby nadepsaní wěřící moji wšecky`{.unchecked lang=ces}   
`penicze nadepsané jistinné 400 kop gr. swrchupsaných úplně a docela na tom na`{.unchecked lang=ces}   
`wšem zboží sobě wybrali a wybrati mohli swobodně, mého a mých dědiców, er-`{.unchecked lang=ces}   
`bów, poručníków i úředníków wšech beze wšeho překazu. A jestli žeby mi to`{.unchecked lang=ces}   
`zbožie nadepsané w té mieře nepřišlo w mé prawé plné swobodne drżenie, tehdy`{.unchecked lang=ces}   
`nadepsanym wóżicim mym slibuji tiemto listem, mü dobrü čistú wčrú kresťanskú,`{.unchecked lang=ces}   
`beze wsie zlé ls, nadepsané penieze 400 kop gr. swrchupsanych oddati a odká`{.unchecked lang=ces}   
`zati na jiném mém zboží swobodném, kdež moci budu, a kteréhož práwě úplně`{.unchecked lang=ces}   
`a swobodně mocen budu; aby nadepsaní moji wéríci wšecky penieze ty jistě na-`{.unchecked lang=ces}   
`depsané, totiž 400 kop gr. nadepsaných na tom jistém mém zboží kterémžkoliwěk`{.unchecked lang=ces}   
`mnů jim odkázaném a oddaném brali, wybierali a zdwihali, bráti, wybierati a zdwi-`{.unchecked lang=ces}   
`hati mohli, padesät kop gr. swrchupsanÿch prawého platu roënieho čistého, poč-`{.unchecked lang=ces}   
`nice o sw. Hawle w plnú dwú letů pořád zběhlů od dánie tohoto listu najprwé`{.unchecked lang=ces}   
`pristiem. A opèt 50 kop gr. nadepsanÿch roënich o sw. Jiří potom i hned najprwé`{.unchecked lang=ces}   
`příštím, a tak potom pti každém sw. Hawlu 50 kop gr. a při každém sw. Jiří`{.unchecked lang=ces}   
`druhých 50 kop gr. swrchupsanych üroënich, wsecka léta budúci požád zbčhlá,`{.unchecked lang=ces}   
`aZ do prawého a plnéóho wybránie swého dluhu swrchu psaného 400 kop gr.`{.unchecked lang=ces}   
`nadepsanych. A kdezkoliwék na mém zboží já napředpsaný Aleš ze Šternberka`{.unchecked lang=ces}   
