[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/119/220.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=119&page=220

***

`196 1437.`{.unchecked lang=ces}   
`kteréž nynější Horníci držie, mají každý s obú stranü po pfieteli`{.unchecked lang=ces}   
`vydati, a jestliže by sě ti smluviti oč nemohli, tehdy toho slo-`{.unchecked lang=ces}   
`vútný Jan z Sútic, mincmeistr, aneb jiný, ktož by na témž úřadě`{.unchecked lang=ces}   
`po něm byl, má býti najvyší rozdielce, vždy hledě k obojie straně`{.unchecked lang=ces}   
`vedle milosti; a toho s obû stranû řekli sú i mají poslušníi býti.`{.unchecked lang=ces}   
`Také jestliže by kto z starých Horníkuov od dánie lista tohoto`{.unchecked lang=ces}   
`pořád čtúce do dne a do roka k svému statku sč, když by mu to`{.unchecked lang=ces}   
`bylo oznámeno, nevrátil, ten potom právo své na tom ztratí, a to`{.unchecked lang=ces}   
`všecko vedle práv a listuov tomu městu od našich předkuov daných`{.unchecked lang=ces}   
`na obec má spadnüti; Zádajice oboji, abychom k těm a takovým jich`{.unchecked lang=ces}   
`dobrovolnym smlüvám svoliti a je ku právóm, svobodám 1 listuom`{.unchecked lang=ces}   
`starodávnym navrátiti rácili milostivé. ProtoZz vidüce v tom obojich`{.unchecked lang=ces}   
`povolnost a poslušenstvie, s dobrým rozmyslem a radi naśi pro`{.unchecked lang=ces}   
`vyzdviżenie toho klenota tiem spóśnejsie Horníky oboje a všecky`{.unchecked lang=ces}   
`svrchu psané již jakžto znova za jednu a jednostajniu obec spo-`{.unchecked lang=ces}   
`lečnie vysazujem, ke všem je právóm jich, svobodám i listóm,`{.unchecked lang=ces}   
`našimi slavné paměti předky jim z staradávna vysazeným a daným,`{.unchecked lang=ces}   
`kterýchž jsú do sie chvíle požívali a požívají, navracujíc a těch`{.unchecked lang=ces}   
`jim i tomu městu mocí naší králevskü v Čechách potvrzujíc tiemto`{.unchecked lang=ces}   
`listem. Ne[Z] ač by pak i v časiech budúcích kterých práv svých`{.unchecked lang=ces}   
`a svobod sě doptati, jichžto nynie pro svú záhubu snad nepama-`{.unchecked lang=ces}   
`tují neb dojiti nemohńi, a je dskami, listmi neb řádným svědomím`{.unchecked lang=ces}   
`okazali, tóch jim také chceme i naši budúcí králové Češčí mají`{.unchecked lang=ces}   
`potvrditi milostivě. A nad to nade všecko vždy k tomu hlediec,`{.unchecked lang=ces}   
`aby měsťané Hor tiem spieše a snázě sě stavěti i hory vyzdvihati`{.unchecked lang=ces}   
`mohli, všecky dluhy minulé a platy světské i duchovnie, křesťan-`{.unchecked lang=ces}   
`ské i židovské, v Hoře aneb u předměstích Horských, kteříž sú`{.unchecked lang=ces}   
`koli byli na všie obci aneb na jmenovitých domiech zvlášče, zdvihli`{.unchecked lang=ces}   
`sme a zdviháme, umořili sme i tiemto listem mofime; a to ve`{.unchecked lang=ces}   
`dsky zemské s radá panskü eheem vložiti, když budú otevřieny.`{.unchecked lang=ces}   
`Také chcem, aby werk grošový neb penézny dělán byl pro potřeby`{.unchecked lang=ces}   
`těch Hor a firdrunk, a steury aby každý týden byly dávány tak,`{.unchecked lang=ces}   
`jakoż sé nám a minemeistrovi bude zdáti. Též pregéře, mincéře,`{.unchecked lang=ces}   
`prenary i jiné k jieh lónóm a právóm [nalvraeujem, které£z na`{.unchecked lang=ces}   
`Vlaském dvoře měli sú a mají i budú mieti. A všecky platy`{.unchecked lang=ces}   
`a sbožie jich i jiných Horníkuov kromě toho, což svrchu psáno`{.unchecked lang=ces}   
`stojí, propúščieme, a jim mocí svrchu dotčenů dáváme tiemto`{.unchecked lang=ces}   
`listem i tvrdíme, buď to na svëdskÿch® neb duchovních kteréž jsú`{.unchecked lang=ces}   
