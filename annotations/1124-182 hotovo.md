[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/1124/182.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=1124&page=182

***

`178 1420 říjen 28 — prosinec 23. Cis. 289, 290.`{lang=ces}   
`289.`{lang=ces}   
`1420, říjen 28. Beroun.`{lang=ces}   
`(Datum in civitate nostra Verona vocata in`{lang=lat}   
`festo beatorum Symonis et Jude apostolorum`{lang=lat}   
`supradicto anno Domini millesimo quadrigente-`{lang=lat}   
`simo vicesimo, regnorum nostrorum anno Hun-`{lang=lat}   
`garie etc. tricesimo quarto, Romanorum undecimo`{lang=lat}   
`et Boemie primo.)`{lang=lat}   
`Zikmund, král římský, uherský a český, zapisuje Ja-`{lang=ces}   
`novi z Ralska a z Vartenberka za 5000 kop grošů čes.`{lang=ces}   
`cisterciácký klášter Hradiště (u Mnichova Hradiště), Dub`{lang=ces}   
`(Český), majetek křížovnického řádu v Praze, ves Pátek,`{lang=ces}   
`majetek strahovského kláštera, a Kostomlaty, které`{lang=ces}   
`patřily svatomářskému klášteru regulovaných kanovníků`{lang=ces}   
`v Kladsku.`{lang=ces}   
`Orig. perg. 40X21—6 cm, lat. K listině jest při-`{lang=ces}   
`věšena na pergamenovém proužku kulatá menší`{lang=ces}   
`pečeť krále Zikmunda z vosku červené barvy.`{lang=ces}   
`Nad textem:`{lang=ces} ` Commissio propria domini regis`{lang=lat} ` — Na zévésném`{lang=ces}   
`proužku petetnim: 12 J.`{lang=ces}   
`Na rubu: XIII. — Nrus 175.`{lang=ces}   
`Lenní reg. ë. 175. — Praz. rep. è. inv. 422, è. rep. 224.`{lang=ces}   
`Regesta imperii XI.,`{lang=lat} ` str. 303, č. 4296. — Sedláček, Zbytky reg.,`{lang=ces}   
`str. 154, č. 1073.`{lang=ces}   
`290.`{lang=ces}   
`1420, prosinec 23. Litoměřice.`{lang=ces}   
`(Datum Leuthmericz sub appenso regio nostro`{lang=lat}   
`sigillo anno Domini millesimo quadringentesimo`{lang=lat}   
`vicesimo die vicesima tercia Decembris, regnorum`{lang=lat}   
`nostrorum anno Hungarie etc. tricesimo quarto,`{lang=lat}   
`Romanorum undecimo et Boemie primo.)`{lang=lat}   
`Zikmund, král římský, uherský a český, nařizuje`{lang=ces}   
`opatu a konventu kláštera tepelského, aby odváděli z krá-`{lang=ces}   
`lovské berně ročně 160 kop grošů Něprovi, řeč. Ducovi,`{lang=ces}   
`z Vařin, purkrabímu nečtinskému, a zbytek Jindřichovi`{lang=ces}   
`z Elsterbergu, nejvyššímu hofmistru`{lang=ces}` (magistro curie),`{lang=lat}   
