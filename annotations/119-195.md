[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/119/195.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=119&page=195

***

`1437. 171`{.unchecked lang=ces}   
`jménem naším i našich budúcich králóv“ Českých potvrzujem`{.unchecked lang=ces}   
`a ovšem pevny i nezrušitedlny činíme, chtiece i tak přikazujíce,`{.unchecked lang=ces}   
`aby žádný těchto našich milostí, daruov, výsad i potvrzenie ne-`{.unchecked lang=ces}   
`rušil ani rušiti směl anebo só jim kterü v&eteónü smělostí proti-`{.unchecked lang=ces}   
`viti, jakoż pokuty neslevitedlné a našeho i budúcích našich krá-`{.unchecked lang=ces}   
`luov Českých jistého a velikého rozhněvánie chtie ujíti. Na po-`{.unchecked lang=ces}   
`tvrzenie toho naši zlatú bullu podobenstvím našie velebnosti vy-`{.unchecked lang=ces}   
`ti&ténü kázali sme?? přivěsiti k tomuto listu, jenž jest dán u Praze`{.unchecked lang=ces}   
`léta od narozenie syna božieho tisícieho čtyřstého tfideátého sed-`{.unchecked lang=ces}   
`mého, v pátek den obrácenie svatého Pavla na vieru, let králov-`{.unchecked lang=ces}   
`ství našich Uherského etc. v padesátém, Římského v sedmimezi-`{.unchecked lang=ces}   
`deietmém, Českého v sedminadetém a ciesařstvie v čtvrtém létě.`{.unchecked lang=ces}   
`[Na ohbu:] Ad mandatum domini imperatoris`{.unchecked lang=deu}   
`Johannes Tussek.`{.unchecked lang=deu}   
`Orig. pergamenový v archivu m. Tábora č. 2. se zlatou bullou zavěšenou`{.unchecked lang=ces}   
`na hedvábné šňůře růžově červené (touž jako při č. 64). Uprostřed majestátu`{.unchecked lang=ces}   
`jest vymalován dosti neuměle nově udělený znak městský. — V témže archivu`{.unchecked lang=ces}   
`chová se ještě druhý originál pergamenový téhož majestátu označený No 1,`{.unchecked lang=ces}   
`k němuž byla přivěšena na hedvábné šňůře běločervené velká pečeť císařská`{.unchecked lang=ces}   
`ze žlutého obyčejného vosku. Pečeť ta krásně zachovaná nyní při majestátu`{.unchecked lang=ces}   
`volně leží. Vyobrazení podává Posse, Die Siegel der deutschen Kaiser und Könige`{.unchecked lang=ces}   
`II, tab. 17, č. 1, 2. — Na rubu tohoto ani předešlého majestátu není znaménka`{.unchecked lang=fra}   
`registraturního. V tomto druhém exempláři majestátu jsou tyto varianty:`{.unchecked lang=fra}   
`а) у dole. — bd) nebo. — € duostojenstvie. — 4) Thábor. — €) vuoli. — /) jsú.`{.unchecked lang=fra}   
`— 9) jé (gie). — h) náměstky. — 9) králóv. — J) Pražské. — k) témi. — ! sé.`{.unchecked lang=fra}   
`— m) budúciech. — ») k žádnému. — ©) coż koli. — ?) bylo by. — 9) Praz-`{.unchecked lang=fra}   
`ském. — 7) požívaného. — s) jmělo. — ©) celé (czelee). — u) zśman. — *) dny.`{.unchecked lang=fra}   
`— x) tof. — 9) ot. — 2) prázny (psáno prázni). — aa) pevnějie (pewniegie). —`{.unchecked lang=fra}   
`bb) se. — co) jakžto. — dd) bielej (bieley). — €9) krajé (kragie). — //) véZemi.`{.unchecked lang=fra}   
`99) pravenymi. — hh) toho. — ii) kteréhož jsú. — 93) sú zde schází. — kk) jasnějie`{.unchecked lang=fra}   
`(iasniegie). — !D i. — mm) obyčejiev. — nn) králuov. — 99) toho náš ciesafskÿ`{.unchecked lang=fra}   
`majestát kázali sme. — Znění majestátu č. 2 (ze zlatou bullou) otisknul Martin`{.unchecked lang=fra}   
`Kolář v roční zprávě reálného gymnasia v Táboře za r. 1867, str. 2, č. I. —`{.unchecked lang=ces}   
`Majestáty tyto byly vydány po úmluvách, které před tím císař s obcí Tábor-`{.unchecked lang=ces}   
`skou byl uzavřel. První úmluva stala se na Třeboni 16. října (v den sv.`{.unchecked lang=ces}   
`Havla) 1436 mezi Oldřichem z Rožmberka a Přibíkem z Klenové jménem cí-`{.unchecked lang=ces}   
`saře a mezi knězem Bedřichem ze Strážnice a opatrnými purkmistrem, kon-`{.unchecked lang=ces}   
`šely i se vší obcí hory Tábor. (Orig. v kníž. archivu Třeboňském; otiskl Pa-`{.unchecked lang=ces}   
`lacký v Archivu Českém III, str. 450, č. 23). V úmluvě té na prvním místě se`{.unchecked lang=ces}   
`stanovilo »aby kněz Bedřich od zákona božieho mocí nebyl tištěn i s tú obcí`{.unchecked lang=ces}   
`svrchu psanü [hory Tábor]|; a jestliže by sě komu zdálo, že by co i s tú obcí`{.unchecked lang=ces}   
`proti písmu neb zákonu božiemu držel, a písmem svatým ukázáno bylo, to mají`{.unchecked lang=ces}   
