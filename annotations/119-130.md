[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/119/130.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=119&page=130

***

`106 1435.`{.unchecked lang=ces}   
`bratry z Krajku o summu peněz, kterou Budejovickym jich ze-`{.unchecked lang=ces}   
`mřelý bratr Lipolt zůstal dlužen, a vypovídá, že bratří z Krajku`{.unchecked lang=ces}   
`ve třech následujících letech mají Budějovickým zaplatiti tisíc a`{.unchecked lang=ces}   
`padesát kop grošů českých.`{.unchecked lang=ces}   
`Wir Albrecht, von gotes gnaden herczog ze Osterreich, ze`{.unchecked lang=ces}   
`Steir, ze Kernden und ze Krain, markgraf ze Merhern und grave`{.unchecked lang=ces}   
`zu Tirol ete., bekennen und tun kund offentleich mit dem brief`{.unchecked lang=ces}   
`von der zuspruch und vordrung wegen, so die erbern, weisen`{.unchecked lang=ces}   
`unser getrewn lieben der burgermaister, der rat und die burger`{.unchecked lang=ces}   
`gemanicleich zu dem Budweis habent gehabt zu unsern lieben`{.unchecked lang=ces}   
`getrewn Kunraten und Janen gebrudern von Kreyg umb zwelf`{.unchecked lang=ces}   
`hundert schokh grosser Prager munss, die in weilent Leupolt`{.unchecked lang=ces}   
`von Kreyg, ir bruder, ist schuldig gewesen nach innhaltung ains`{.unchecked lang=ces}   
`geltbriefs daruber gegeben, derselben zusprueh, vordrung und stoss,`{.unchecked lang=ces}   
`so zu baiderseitt hinder uns sind gegangen und uns gelobt ha-`{.unchecked lang=ces}   
`bent, genezleieh stetz halten, was wir darumb sprechen, daz wir`{.unchecked lang=ces}   
`darumb zwischen in wolbedechtleich naeh unser ret rat auzge-`{.unchecked lang=ces}   
`sprochen haben und sprechen auch wissentleich mit dem brief,`{.unchecked lang=ces}   
`als hernach geschriben stet: Des ersten sprechen wir, daz uns`{.unchecked lang=ces}   
`die egenanten vom Budweis den vorgenanten geltbrief, den sie`{.unchecked lang=ces}   
`von weilent Leupolten von Kreyg habent, an vereziehen sullen`{.unchecked lang=ces}   
`ubergeben und den zu unsern handen antwurten. Daengegen sullen`{.unchecked lang=ces}   
`dieselben von Kreyg fur sieh und ir erben die vom Budweis ver-`{.unchecked lang=ces}   
`sorgen mit aim newn geltbrief umb tausent schokh und fumfczig`{.unchecked lang=ces}   
`sehokh gross Prager munss naeh innhaltung ainer notel, die man`{.unchecked lang=ces}   
`in in unserr kanezley sol machen, denselben geltbrief uns die von`{.unchecked lang=ces}   
`Kreyg, so der gevertigt wirt, auch ubergeben sullen zu unsern`{.unchecked lang=ces}   
`handen; dieselben zwen brief wir den baiden tailn gen einander`{.unchecked lang=ces}   
`ubergeben wellen. Denn von der beezalung wegen der vorgenan-`{.unchecked lang=ces}   
`ten summe gross, sprechen wir, daz die von Kreyg derselben`{.unchecked lang=ces}   
`summe gross den vom Budweis an vereziehen beezaln sullen fumf-`{.unchecked lang=ces}   
`ezig sehokh gross, und die andern summ gross sullen sie in be-`{.unchecked lang=ces}   
`ezahlen zu den nachgeschrieben zeiten: Des ersten auf sant Jorgen`{.unchecked lang=ces}   
`tag des gegenwurtigen iars drewhundert schokh drewunddreissig`{.unchecked lang=ces}   
`schokh und zwainezig gross; item auf sant Jorgen tag des`{.unchecked lang=ces}   
`kunftigen vierezehenhundertisten und sechsunddreissigisten iars`{.unchecked lang=ces}   
`aber drewhundert schokh drewunddreissig sehokh und zwainezig`{.unchecked lang=ces}   
