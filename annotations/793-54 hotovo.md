[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/793/54.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=793&page=54

***

`54 C. V. Wijpisy ze starých desk zemských r. 1422.`{lang=ces}   
`4.`   
`Hájek z Hodétína zapisuje Wilemowi z Prosteho a Bernartowi z Byšic platy: dědičné`{lang=ces}   
`w Olešné a w Daménicich.`{lang=ces}   
`1422, 21 Febr. (DD. XX, 69.)`{lang=lat}   
`Já Hájek z Hodětína, purkrabie na Zwiekowč, i s swymi erby, wyznáwáme`{lang=ces}   
`tiemto listem obecně přede wšemi lidmi, ktoZ jej uziie, nebo čtúce uslysie, Ze`{lang=ces}   
`jakož sem byl peněz zajal pro potřeby králowy, pána swého milostiwého , u pana`{lang=ces}   
`Pušky dobré paměti, tu sem byl za ty penieze zastawil w rukojemstwie slowutné`{lang=ces}   
`panose Wiléma z Prostého a Bernarta z Byšic podlé jiných rukojmí; tu sem jich`{lang=ces}   
`z. loho rukojemstwie odwaditi nemolil, neż oni si st sami swymi penózi odwadili.`{lang=ces}   
`A já sem jim i jich erbóm na tom dal, i mocí tohoto listu dáwám i dědičně po-`{lang=ces}   
`stupuji, puol dewáta lana w Olešné w té wsi, ježto leží pod Načeracem, na lidech`{lang=ces}   
`usedlých, kteříž platí a platiti mají platu roénieho s lanu do roka sedmdesát gr.,`{lang=ces}   
`z těch lanów jeden lan plati bez dwů sedmdesát gr. a mlýn dwadceti gr., a wDa-`{lang=ces}   
`měnicích w té wsi tří lany, kteříž platí také a platiti mají platu ročnícho každý`{lang=ces}   
`s lanu do roka kopu a pět gr., na tom wšem sbožie, jakoZ mi sé dédiénó a spra-`{lang=ces}   
`wedliwé dostalo po smrti nebožce paní Jitky sestry mé, a Dluhomilowé někdy`{lang=ces}   
`řečené, se wším plným panstwim i se wsemi plnymi požitky; sobě ji tu ani swym`{lang=ces}   
`erbóm nizádného práwa nezuostawuji, než tak aby oni swrchupsani Wilém a Ber-`{lang=ces}   
`nart i jejich erbowé mohli to prwépsané sbozie prodati, dati, zastawili a uëiniti`{lang=ces}   
`jako s swym wlastnim dèdictwim. À ten swrchupsanÿ plat, což jeho muož býti,`{lang=ces}   
`dal sem jim kopu platu po desieti kopäch gr. na jich dielech; a jestliZeby co`{lang=ces}   
`wiece mimo tyto swrchupsané penieze za to rukojemstwie znostalo, to mi mají`{lang=ces}   
`nawrâtiti; pakliby sè co nedostalo, to jim mám i slibuji dodati bez jich beze`{lang=ces}   
`wsie škody. A to prwépsané sboZie mám jim zprawowati i &islo uéiniti pred. kre-`{lang=ces}   
`stany i pied židy, před sirotky 1 pred wény, pred swétskymi i pred duchownimi,`{lang=ces}   
`tak jakož práwo a obyčej jest w zemi České, a to jim i jich erbôm wloZiti a we-`{lang=ces}   
`psati ku prawému dědictwie na prwnie suché dní nebo na druhé nebo konečně`{lang=ces}   
`na třetí, když budú dsky zemské u Praze otewřieny ; pakli' jiné práwo kteróżkoli-`{lang=ces}   
`wèk w této České zemi byloby, žeby desky zemské byly zrušeny, ale týmž práwem`{lang=ces}   
`jim mam i slibuji zprawowati swymi náklady a bez jich wšie škody. A ktozby`{lang=ces}   
`tento list měl s swrchu psaného Wiléma neb Bernarta dobrú woli, ten má i mieti`{lang=ces}   
`bude túž plnú moc i též plné práwo ke wšem wěecem w tomto listu psaným, jakož`{lang=ces}   
`oni sami. A toho wšeho pro lepsie swèdomie, jistost 1 potwrzenie, swü sem wlastní`{lang=ces}   
`pečet k tomuto listu piiwesil s swa dobra woli i s swym dobrym wédomim, a na`{lang=ces}   
`swedomie k mé prosbé pan Hasek ze Zwieielic, pan lindrich z Dubé a Jindrich`{lang=ces}   
