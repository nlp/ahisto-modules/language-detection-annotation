[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/529.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=529

***

`10`   
`15`   
`20`   
`30`   
`Urfunden zum Jahre 1428. `{lang=deu} `517`   
`umbe brugkendelin und holz zu den weren 9 gr. — Item als`{lang=deu}   
`meine herrn den houptman von Erfurd uff das ander mit sinen`{lang=deu}   
`gesellin zu geste botin, da kost das essen an spise und an tranke`{lang=deu}   
`1 sch. 151% gr.)`{lang=deu}   
`[Bl. 69b] ltem des suntages noeh unsers herrn hymmelfard`{lang=deu}   
`[Mai 16]:`{lang=deu}   
`Item dem gardian vor kalkstein 3 mr. gr. -— ltem`{lang=deu}   
`Hannus Weider, Peter Ounczen und Nickel Newman kein`{lang=deu}   
`der Lobaw mit landen und steten zu tage noch deme abscheiden`{lang=deu}   
`zu Legenicz, das [man] mit den fursten in der Slesia mit den`{lang=deu}   
`Bresslern, mannen und steten im lande zur Sweidniez zu tage`{lang=deu}   
`kein dem Jauwer kommen sulde von eynis feldes und und umme`{lang=deu}   
`hulffe, ob dy keczer in der Slesia blebin weren, und ander ge-`{lang=deu}   
`brechen, 50 gr. — Item umbe eichin zu den beuwen 7 gr. —`{lang=deu}   
`Item umbe delin zu den weren in dy parchan 3!/e sol. 2 gr. —`{lang=deu}   
`Item umbe mos zu dem schucze in dem graben*) 5 gr. — Item`{lang=deu}   
`umbe brugkendelin 17 gr. — Item Feitschcz und Andris kein`{lang=deu}   
`Budessin zu beleiten herrn Dyterich Kruczburg thumprobist zu`{lang=deu}   
`Budessin 8 gr. — Item einem louffindin botin kein Budessin`{lang=deu}   
`mit herzog Heinrichs brives abeschrift, als her myne herrn zu`{lang=deu}   
`tage lut kein der Sprottau in den phingist hihge tagen, 4 gr. —`{lang=deu}   
`Item Pauwel Rinkingisser und dem statschreiber kein dem Jauwer`{lang=deu}   
`und furbas kein Legenicz zu dem tage zu den fursten, mannen`{lang=deu}   
`und stetin zur Sweidenicz im lande als von einer eynunge wegen`{lang=deu}   
`obir dy keczer, do dy von Budissin mit uns nicht zu tage woren,`{lang=deu}   
`4 sch. minus 1 fert. — [Bl. 70a] Item umbe delen zu den weren`{lang=deu}   
`in dy parchan 1 mr. 6 gr. — Item umbe zymmer zu den weren`{lang=deu}   
`9 gr. 4 pf. — Item Pauwel Rinkingisser kein der Zittau zu be-`{lang=deu}   
`sehin dy camerbochsse, das myne herrn ouch eine sulche donoch`{lang=deu}   
`mochten lossin gissin, und ouch kein Reichinbach, do man die`{lang=deu}   
`5 gefangin her ein dy stad furte, die her Luter gefangen hatte,`{lang=deu}   
`20 gr. — Item umbe zymmer zu den weren und umbe eichin`{lang=deu}   
`16 gr. 4 pf. — Item dem bochssenmeister von der Zittaw, als`{lang=deu}   
`myne herrn en besant hattin, das her en eine nuwe camerbochsse`{lang=deu}   
`1) Die €tntragung ift geftricber, f. oben. S. 508, 26 ff.`{lang=deu}   
`2) Das Moos diente jedenfalls dazu, die Schutze, 0. b. oie Breiter zum Auf:`{lang=deu}   
`ftanen des. Waffers dicht zu machen.`{lang=deu}   
