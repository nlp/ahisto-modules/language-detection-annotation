[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/42.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=42

***

`30 Urkunden zum Jahre 1420.`{lang=deu}   
`wie is bliben wirt, so wil ichs euwer liebe gerne zu wissen`{lang=deu}   
`thuen. Gegebin am montage noch esto mihi.`{lang=deu}   
`/[ Rückseite] Den erbern und Peter Raster genant Molschriber,`{lang=deu}   
`wolwiesin burgermeister und mitburger zu Bresslaw.`{lang=deu}   
`5 rathmann zu Gorlitz, mynen`{lang=deu}   
`lieben gunnern, d[ari] d[ebet].`{lang=deu}   
`Molschreiber von. Bresslaw.`{lang=deu}   
`Das Jahr ergiebt {ich aus der Unwefenheit des Sigmund in Breslau. Daf`{lang=deu}   
`%afob Baumgart aus dem Gefängnis los fam, fieht man aus den Rats:`{lang=deu}   
`10 rednungen unter dem 2. März 1420 (f. S. 21, 2), vergl. auch) Ratsrednungen`{lang=deu}   
`unter dem 17. Febr. 1420.`{lang=deu}   
`[1420]. März 7. Breslau.`{lang=deu}   
`K. Sigmund befiehlt den Sechsstädten mit ihren grössten Büchsen`{lang=deu}   
`15 und mit ihrem Volke sich bereit zu halten, um auf das Gebot`{lang=deu}   
`ihres Landvogtes zu ihm zu stossen. Ш`{lang=deu}   
`Aus Milichfche Bibl. mspt. fol. 217, p. 117, Original oder gleich:`{lang=deu}   
`zeitige Abfchrift. Erwähnt Saufiger IMagazin-1774 5. 150. Danach`{lang=deu}   
`Dalady, MrfunoL Beitr. I. S. 21. — Oberlauftt. Urfundenverzeichnts`{lang=deu}   
`20 IL ©. 2. Кой diplomatarium I. 5. 56. E`{lang=deu}   
`Wir Sigesmund von gotes gnoden Remischer konig, zu allen`{lang=deu}   
`zeiten merer des reichs, unde konig zu Ungarn, zu Behem, Dal-`{lang=deu}   
`macien, Crowacien konig, empiten den burgermeistern, rath-`{lang=deu}   
`mannen unde burgern gemeinlichen unserer stete zu Budessin,`{lang=deu}   
`% zu Gorlitz, zur Zitaw, zu Camencz, zu Lubaw, zu Luban unsern`{lang=deu}   
`liben getrawen unsere gnode unde alles gut. Liben getrawen,`{lang=deu}   
`wenne wir etliche widerwertige in der crone zu Behem wollen`{lang=deu}   
`haben zu straffen unde auch nu bestellet haben wider sie zu`{lang=deu}   
`zihen, dorumme gebiten wir euch ernstlichen mit diesem brife,`{lang=deu}   
`30 das ir euir buchsen, was ir denne der grosten in euirn steten`{lang=deu}   
`habet, offladen unde euch gonz derzu schicken sullet, wenne`{lang=deu}   
`euch de[r] edel Hlawoezs von der Leipen, euir houptman, enpiten`{lang=deu}   
`wirt, das ir dann mit euirn gezeuge unde mit euirm folk bereit`{lang=deu}   
`seit zu uns zu komen. Das ist uns wol von euch zu dank unde`{lang=deu}   
`x wollen das auch kein euch furbas gnediclich irkennen. Gegeben`{lang=deu}   
`zu Breslaw am donnerstage noch dem suntage, als man in der`{lang=deu}   
`heiligen kirchen reminiscere, unserer reiche des Hungerischen eto.`{lang=deu}   
`in deme 23. jore und des Remischen in deme zehenden jore.`{lang=deu}   
`Die Górlier Ratsrediungen erwähnen den Brief den 2. Wlacz 1420 (j. oben`{lang=deu}   
`40 S. 21, 2).`{lang=deu}   
