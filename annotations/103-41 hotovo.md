[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/41.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=41

***

`10`   
`15`   
`19`   
`ex`   
`30`   
`35`   
`Urfunden jum Jahre 1420.`{lang=deu} `29`   
`[1420]. Febr. 1. Münsterberg.`{lang=deu}   
`Johannes und Heinrich, Herzöge von Miinsterberg, benach-`{lang=deu}   
`richtigen die Breslauer Ratmänner, dass der Verhandlungstag,`{lang=deu}   
`den sie (die Herzüge) am Freitag nach Dorothea [8. Febr.]`{lang=deu}   
`mit den Breslauern haben werden, auch von den Sechsstüdten`{lang=deu}   
`beschickt werden wird.`{lang=deu}   
`Aus Stadtarchiv Breslau, Correfp., Orig. Pap.`{lang=deu}   
`[1420]. Februar 19. Breslau.`{lang=deu}   
`Ein Breslauer Bürger berichtet den. Gürlitzern von den Mass-`{lang=deu}   
`regeln des K. Sigmund gegen die Aufrührer des Jahres 1418.`{lang=deu}   
`Jus Wültbfde Bibliothef mspt. fol. 217 p. 212. Original mit Siegel:`{lang=deu}   
`fpuren auf der Rückfeite. Gedruckt von Griinhagen, Seitfchrift des`{lang=deu}   
`Dereins fiir Gejchichte und Altertum Schlefiens ХТ 5. 194 f.  Deral.`{lang=deu}   
`Griinhagen, Huffitenfdmpfe S. 16 f.`{lang=deu}   
`Meinen früntlichen grüs mit allir behegelichkeit zuvor. Be-`{lang=deu}   
`sundern lieben herren u. gunner, euwir liebe tue ich zu wissin,`{lang=deu}   
`als ir mir geschreben hat mit eyner zedil in euwirm briffe,`{lang=deu}   
`euwern dyner zu undirwiesin, wo her die funde, den die brife`{lang=deu}   
`sprechen, zo wisset, der briff, der do spricht Jentsch, und der-`{lang=deu}   
`selbe man ist iczunt nicht eynhemisch noch was in der stat zu`{lang=deu}   
`der selben zeit, sundir wenn her kommit und in der stat sein`{lang=deu}   
`wirt, so wil ich en ym gerne antwortten von euertwegen; adir`{lang=deu}   
`der andir briff, der.do spricht Bomgarthen, zo wisset, das her`{lang=deu}   
`gefangen ist und die stat helt en ynne von des koniges wegen`{lang=deu}   
`und zu ym nicht kommen mochte; adir so ym got aus gehilfft,`{lang=deu}   
`zo wil ich en ym gerne antworten. Auch lieben herren, welde`{lang=deu}   
`ich euch gerne icht vorschriben von zitungen, wie is zu Broslaw`{lang=deu}   
`stunde adir bliben worde, ich kan sein nicht gewissin. Noch`{lang=deu}   
`kan euch nicht geschriben von worer zitunge, sundir wissit, das`{lang=deu}   
`unser gnediger herre der konig hot lossin vohen vil leute aus`{lang=deu}   
`der” gemeyne, und was her domit menit, das kan man noch nicht`{lang=deu}   
`gewissin, sundir eyn teil synt auskommen und hot sie lossin`{lang=deu}   
`aüsburgen, adir die andern wil man nicht ausgeben zu burgen`{lang=deu}   
`und was her mit en machen wil, das wéis unsers herren gnode ^`{lang=deu}   
`wol; nicht me kan ich euch geschriben zur desir zeit, sundir`{lang=deu}   
`wenne ich icht hore adir dirfare von wohrhaftigen sachen und`{lang=deu}   
