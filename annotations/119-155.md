[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/119/155.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=119&page=155

***

`1436. 131`{.unchecked lang=ces}   
`Obsaženo v potvrzovacím listu kr. Jiřího neznámého data, jehož útržek se`{.unchecked lang=ces}   
`chová v archivu Musea král. Českého. Zmínka též v registrech zápisů z r. 1453`{.unchecked lang=ces}   
`v Arch. Českém I, str. 504, č. 55, odkudž doplněno datum. — Sedláček, Zbytky`{.unchecked lang=ces}   
`reg., 6. 1377. — Zlomek listu Karlova v. v Privilegiích II, č. 372.`{.unchecked lang=ces}   
`* Cís. 88.`{.unchecked lang=lat}   
`1436, 18. října. V Praze.`{.unchecked lang=lat}   
`Císař. Sigmund potvrzuje měšťanům Klatovským výsady,`{.unchecked lang=ces}   
`které jim udělili předešlí králové Čeští a jmenovitě otec jeho`{.unchecked lang=ces}   
`Karel IV. — Datum v Praze na den svatého Lukáše evangelisty`{.unchecked lang=ces}   
`léta tisícého čtyřistého třidcátého šestého.`{.unchecked lang=ces}   
`Zmínka v konfirmaci kr. Ferdinanda I. d. na hradě Pražském 24. září 1547,`{.unchecked lang=ces}   
`v registrech majestátův z l. 1545— 1547 č. 285 na 1. 101 v c. k. šlechtickém ar-`{.unchecked lang=ces}   
`chivu ve Vídni. — Jiná zmínka o majestátu tom jest v rukopise ze začátku`{.unchecked lang=ces}   
`XVII. stol. v archivu Musea král. Českého zvaném »Registrum a poznamenání`{.unchecked lang=ces}   
`majestátův a privilegií, která někdy městu Klatovům byla a jsou dána (zůstá-`{.unchecked lang=ces}   
`vají na ten čas v krabici velké nad almarou neb deposicí akt všelijakých)«,`{.unchecked lang=ces}   
`kdež se praví: »Král Zigmund, syn Karla IV. a bratr krále Václava, potom`{.unchecked lang=ces}   
`císařem, že tento ignavus bratr do říše přijížděti, bílého českého piva, pěkného`{.unchecked lang=ces}   
`fraucimoru opustiti nechtěl, učiněn jsa, na Konstanském concilium m. Jana`{.unchecked lang=ces}   
`z Husinee a m. Jeronyma z Prahy upáliti, přítomný sedě, dovolil, tvrdí přede-`{.unchecked lang=ces}   
`šlých králův a otee svého majestáty. Neměl štěstí žádného, z Čech vyhnán, od`{.unchecked lang=ces}   
`Turka poražen, jsa králem také Uherským, kdy Žižka jinak Jan z Trocnova`{.unchecked lang=ces}   
`z Kalicha biskupy, mnichy v klášteřích navštěvoval, palcátem světil ete«. —`{.unchecked lang=ces}   
`V registrum tom děje se dále zmínka o jiném majestátu, jehož snad přepis tehdy`{.unchecked lang=ces}   
`(asi r. 1603) méli v Klatovech: »Král Zigmund po vyhnání jeho z království od`{.unchecked lang=ces}   
`obce všech tří stavů tehda povstalé, když zase do Čech po nešťastném s Turkem`{.unchecked lang=ces}   
`potkání se vracoval, činí milost všem městům království Českého a při tom`{.unchecked lang=ces}   
`pokoj, jak tehdáž k míru přivedeno býti mohlo, utvrzuje«. Slovy těmi míní se`{.unchecked lang=ces}   
`bezpochyby majestát d. v Jihlavě 20. července 1436, o němž již výše pod č. 74`{.unchecked lang=ces}   
`bylo poznamenáno, že jej císař vydal asi také některým královským městům`{.unchecked lang=ces}   
`venkovským.`{.unchecked lang=ces}   
`Čís. 84.`{.unchecked lang=ces}   
`1436, 9. listopadu. V Praze.`{.unchecked lang=ces}   
`Císař Sigmund potvrzuje a obnovuje rychtáři, kmetům, pří-`{.unchecked lang=ces}   
`sežným i vší obci města Loun dva listy krále Jana, jimiž jim`{.unchecked lang=ces}   
`bylo povoleno vybírati mýto.`{.unchecked lang=ces}   
`Sigismundus, dei gracia Romanorum imperator semper au-`{.unchecked lang=ces}   
`gustus ac Hungarie, Bohemie, Dalmacie, Croacie ete. rex. Notum`{.unchecked lang=ces}   
