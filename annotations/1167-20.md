[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/1167/20.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=1167&page=20

***

`držeti a zachovati i ostříhati s pomocí nestvořené a na věky po-`{.unchecked lang=ces}   
`žehnané trojice svaté. Amen. Tak Pán Buoh dej.`{.unchecked lang=ces}   
`Výstraha Hradeckých Žižkovi před úkladným vrahem;`{.unchecked lang=ces}   
`z 22. listopadu 1423.`{.unchecked lang=ces}   
`Truchlivé temnoty ostatního Žižkova života, z něhož není již jeho listů, oza-`{.unchecked lang=ces}   
`řuje aspoň poněkud zajímavý tento dopis duchovního vůdce pozdějších Sirotků`{.unchecked lang=ces}   
`a vlastního vládce hlavního jejich města, kněze Ambrože, jakož i tamního hejt-`{.unchecked lang=ces}   
`mana Jana z Vícemilic, zv. féz Bzdinkou, Je krásným svědectvím neobyčejně`{.unchecked lang=ces}   
`oddanosti a lásky, jaké Žižka požíval. Uklad zosnován byl neispíie předním`{.unchecked lang=ces}   
`stranikem prohnaného mistra v zékernych pletichách, podlého Zikmunda, Janem`{.unchecked lang=ces}   
`Mésteckym.`{.unchecked lang=ces}   
`Pán Buoh všemohúci rač býti s tebú, se všemi bratfími věrnými`{.unchecked lang=ces}   
`: s némi hfiénÿmi svû svatú milostí a pomocí. Bratře Žižko i bratří`{.unchecked lang=ces}   
`naši nejmilejší! Věz, žef jsme jednoho Opočenských strany jali,`{.unchecked lang=ces}   
`vězně dosti znamenitého, kterýž jesti nás za jisté zpravil, že již`{.unchecked lang=ces}   
`jeden jest s tebú u vojště, kterýž té mé zamordovati; a za to má`{.unchecked lang=ces}   
`ještě třiceti kop vziti, a již deset kop hotových vzal. Pak toho`{.unchecked lang=ces}   
`jistého mordéře zná Pavel s černů hlavú kadeřavů, tohoto listu`{.unchecked lang=ces}   
`ukazatel. A protof sme my jeho k tobě vypravili, kterýž tě má`{.unchecked lang=ces}   
`všeho úplně zpraviti, kdož to jednají a kterak to má jednáno býti,`{.unchecked lang=ces}   
`a kto to má učiniti, toho tobě ukázati. Kterémužto Pavlovi pro-`{.unchecked lang=ces}   
`sime, aby jemus věřil, což s tebů od nás o té věci mluviti bude.`{.unchecked lang=ces}   
`Pán všemohůcí rač tě zachovati k své chvále a k prospěchu věr-`{.unchecked lang=ces}   
`ným obcem. Dán v Hradci nad Labem v pondělí v třetí hodinu`{.unchecked lang=ces}   
`v noci před sv. Kateřinů, ruků tvého kněze Ambrože.`{.unchecked lang=ces}   
`Janek Hvězda, purkmistr, konšelé i tvój kněz Ambrož.`{.unchecked lang=ces}   
`Husitská hymna.`{.unchecked lang=ces}   
`Slavná píseň, pred jejímž zpěvem pouhým se dávali křižáci na útěk, nepo-`{.unchecked lang=slk}   
`chází sice od Žižky, nýbrž od neznámého husitského kněze. Než důvěrný. její`{.unchecked lang=slk}   
`20`{.unchecked lang=ces}   
