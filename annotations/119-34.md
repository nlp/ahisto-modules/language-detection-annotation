[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/119/34.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=119&page=34

***

`10 1420.`{.unchecked lang=ces}   
`Čís. 8.`{.unchecked lang=ces}   
`1420, 4. prosince. Na Horách Kutných.`{.unchecked lang=ces}   
`Král Sigmund zapisuje Čáslavskému rychtáři Ondřejovi a`{.unchecked lang=ces}   
`Václavu Finderovi z Čáslavě i jejich dědicům vsi kláštera Sedlec-`{.unchecked lang=ces}   
`kého Zbislav, Rohozec a Kunice se vším příslušenstvím ve čty-`{.unchecked lang=ces}   
`řech stech kopách grošů českých, jež jim byl dlužen.`{.unchecked lang=ces}   
`Nos Sigismundus, dei gracia Romanorum rex semper augustus`{.unchecked lang=ces}   
`ac Hungarie, Boemie, Dalmacie, Croacie ete. rex. Memorie com-`{.unchecked lang=ces}   
`mendantes tenore presencium significamus guibus expedit univer-`{.unchecked lang=ces}   
`sis, quod nos quasdam villas Sbislaw, Nohosen“ et Cuniez claustri`{.unchecked lang=ces}   
`Zedlieensis simul cum omnibus earum proventibus et pertinenciis,`{.unchecked lang=ces}   
`signanter vero piscinis, slvis àáe utilitatibus quibuslibet, iustis ut`{.unchecked lang=ces}   
`puta et legittimis, quovis nominis voeabulo vocitatis ad easdem`{.unchecked lang=ces}   
`villas de iure speetantibus eum pleno dominio earundem nil inde`{.unchecked lang=ces}   
`excipiendo fidelibus nostris Andree iudiei et Venceslao Fynder`{.unchecked lang=ces}   
`de Czaslauia, tum causa tutele, proteccionis et conservacionis ta-`{.unchecked lang=ces}   
`lium villarum, tum etenim pro quadringentis sexagenis grossorum`{.unchecked lang=ces}   
`Boemiealium, quibus ipsis in debitis obligamur, pignori duximus`{.unchecked lang=ces}   
`obligandum, ymmo obligamus et impignoramus per presentes;`{.unchecked lang=ces}   
`ita tamen, quod quandoeumque nos aut nostri suecessores, re-`{.unchecked lang=ces}   
`ges ut puta Boemie, vel alii quorum interfuerit, de premissa`{.unchecked lang=ces}   
`summa peeunie eisdem Andree iudiei et Venceslao Fynder`{.unchecked lang=ces}   
`aut ipsorum heredibus satisfecerimus aut satisfecerint, extune`{.unchecked lang=ces}   
`ipsi aut ipsorum heredes rehabitis premissis quadringentis`{.unchecked lang=ces}   
`sexagenis grossorum easdem villas simul] cum predietis earun-`{.unchecked lang=ces}   
`dem utilitatibus et pertineneiis quibuslibet predicto elaustro Ze-`{.unchecked lang=ces}   
`dlieensi sine omni difficultate remittere et resignare debeant`{.unchecked lang=ces}   
`et teneantur. Et quis vel qui presentes litteras eum eorundem`{.unchecked lang=ces}   
`Andree iudieis et Veneeslai Fynder bona voluntate habuerit seu`{.unchecked lang=ces}   
`habuerint, illi vel illis ius eompetit omnium premissorum harum`{.unchecked lang=ces}   
`nostrarum, quibus sigillum nostrum appensum est, testimonio`{.unchecked lang=ces}   
`litterarum, quas dum nobis in specie fuerint reportate, sigillo`{.unchecked lang=ces}   
`nostro maiori imperiali faeiemus eommuniri. Datum in Montibus`{.unchecked lang=ces}   
`Chuttnys in festo beate Barbare virginis et martyris anno Do-`{.unchecked lang=ces}   
`mini millesimo quadringentesimo vigesimo, regnorum nostrorum`{.unchecked lang=ces}   
