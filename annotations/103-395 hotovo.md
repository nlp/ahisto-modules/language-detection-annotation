[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/395.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=395

***

`30`   
`Urkunden zum Fabre 1427.`{lang=deu} `.. 888`   
`getan hat, das man in dem garten gemauert hat und die steine`{lang=deu}   
`dorynne gelegen haben. — Item Herman Schultes und Jorge`{lang=deu}   
`Canicz gen der Lobaw zu lande und steten umb einen vorweser,`{lang=deu}   
`der das volk regyren sulde, haben vorzert 26 gr. 9 pf. — Item`{lang=deu}   
`3 gr. Schelechen, das her uns ein brieff brochte von Budissen. —`{lang=deu}   
`Item 1 gr. vor ungemacht!) waynsmer zu den karren. — Item`{lang=deu}   
`Hannus Weider und Paul Rynkengisser gen der Lobaw zu lande`{lang=deu}   
`und steten umb die reise, ap man zu kurfursten zihen sulde`{lang=deu}   
`adir ap wir selbir vor die Leipe zihen wolden, 31 gr. — Item`{lang=deu}   
`des sunabendes am obende unser lieben frauen assumptionis?)`{lang=deu}   
`dem gesinde zu vortrinken zum heiligen obende 3 mr. gr. —`{lang=deu}   
`Item umbe 6 steine koppher zu bochssin 9 sch. 5 gr. —`{lang=deu}   
`[ В!. 174а] Item hofslag 42!/s gr. — Item vor eine haue 5 gr. —`{lang=deu}   
`Item vor 6 radeborn zu binden 6 gr. — Item fir hauen gestelt?)`{lang=deu}   
`4 gr. — Item vor clammern und sparnail 4 gr. — Item vor`{lang=deu}   
`zwene grosse nayl 2 gr. — Item dem marsteller vor fir wochenlon`{lang=deu}   
`.16 gr. — Item dem jungen 2 gr. zu vertrinken. — Item Nickel`{lang=deu}   
`dem stalknechte vor fir wochenlon 1 fert., 2 gr. zu vertrinken. —`{lang=deu}   
`Item meister Gregor vor ein slos mit zweien slosseln 2 gr. —`{lang=deu}   
`Item 3 gr. vor stegereifen. — Item 4 gr. vor ein slos, das man`{lang=deu}   
`vorslet*), und kripptuch. — Item vor ein zaum, ein par steig-`{lang=deu}   
`leder, vor vorbuge?) und zweie gezoge®), ein par selhalssen?)`{lang=deu}   
`und nerymen?) 21 gr. — Item vor zwene filze 5 gr. — Item`{lang=deu}   
`vor zweie follen?) 2 gr. — Item vor ein par kegelleder!?) 8 er. —`{lang=deu}   
`Item Hannus dem waynknechte vor fir wochlon 16 gr., 2 gr. zu`{lang=deu}   
`vertrinken. — Item dem Nickel waynknechte 5 gr. — Item dem`{lang=deu}   
`andern knechte 4 gr. — /BI. 174b] Item dem buchssenmeister`{lang=deu}   
`6 gr. zum heiligen obende zu vertrinken und 4 gr. den gesellen,`{lang=deu}   
`die im zwu bochsen haben geholffen gissen. — Item Merten`{lang=deu}   
`dem boten, das her die gefangen ausgehalden hat, 18 gr. —`{lang=deu}   
`T) unverfälfht.`{lang=deu}   
`?) Da bie assumptio Marie 1427 auf einen Sreitag fülft, ift die Datierung un-`{lang=deu}   
`richtig. Gemeint ift am obinde der kirmesse noch assumpt. Marie, alfo ber`{lang=deu}   
`16.`{lang=deu}   
`Ananft.`{lang=deu}   
`3) geftählt.`{lang=deu}   
`4) vor{chlägt.`{lang=deu}   
`Bruftriemen der Pferde.`{lang=deu}   
`Gefchirre.`{lang=deu}   
`Tragriemen, die über den Hals genommen werden.`{lang=deu}   
`8) Doch wohl = genähte Riemen.`{lang=deu}   
`9) = phul Kiffen?`{lang=deu}   
`1) — gegenleder, Zuariemen?`{lang=deu}   
`1`{.error}   
`SN’ Ne”`{lang=deu}   
`25`   
