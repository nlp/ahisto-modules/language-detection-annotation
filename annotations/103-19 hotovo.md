[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/19.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=19

***

`Or`
`10`
`20`
`25`
`35`
`Urfunden zum Jahre 1419. ‚7`{lang=deu}  
`[Er. IV. Bl. 28507`{lang=deu} `Sabbato post Michahelis`{lang=lat} `[Oktober 7]:`{lang=deu}  
`Der statschreiber mit landen unde stetin kein Ungern`{lang=deu}  
`unsern gnedigen herren den konig noch unsers des koniges`{lang=deu}  
`tode zu besuchen, als sich zu thuen gebórte, woren ussen fumf ganze`{lang=deu}  
`wochen in fremden landen 11 sch. Mit den selbin quam her`{lang=deu}  
`zu uns her Wend von Ileburg in unsers herrn des koniges gewerke`{lang=deu}  
`mit ern Christoff von Ghersdorff, die worden geert mit weyne`{lang=deu}  
`unde bire 14 gr. Doselbist worden die land unde stete abir`{lang=deu}  
`geert mit wyne unde bire 12 gr. Item als her Wend von Ileburg |`{lang=deu}  
`mit landen unde steten von Ungern quomen, do muste [wir] in -`{lang=deu}  
`schuezen enkeyn senden durch ferlichkeit willen mit 16 pherden`{lang=deu}  
`ll) mr. — Dornoch quomen die von Bunczlaw her zu uns mit`{lang=deu}  
`Heincze Russendorf, worden geert 8 gr. — [BIl. 283 b] Bartholo-`{lang=deu}  
`meus Ebirhard, Niclos Wyder, der statschreiber mit landen unde`{lang=deu}  
`steten kein der Lobaw zu tage mit ern Wende von Ileburg,`{lang=deu}  
`ern Christoff von Ghersdorff zu deme foite, als man von`{lang=deu}  
`Ungern komen was, mit manchirlei sachen cum curru 1 sch.`{lang=deu}  
`minus 3 gr. Doselbist schenkite man ern Wende von Ileburg`{lang=deu}  
`ein legil mit wyne 8 gr. — Einen boten kein deme Luban`{lang=deu}  
`2 gr. — Einen bothen kein Budessin mit ern Wendes brifen`{lang=deu}  
`von llenburg vorbas kein Meissen zu senden 4 gr. — Item`{lang=deu}  
`den mnüehn reitherknechten obir den somer vor parchan zu`{lang=deu}  
`underjopen 1 sch. 16 gr. — Item in die hute off die scharen!)`{lang=deu}  
`und unsere frunde zu Budessin woren mit Mathes Kezer durch`{lang=deu}  
`manchirleie besorgunge wille 1 sch.`{lang=deu}  
`BGörliger Ratsrechnungen.`{lang=deu}  
`BV.`{lang=deu}  
`[Bl. 2b]`{lang=deu} `Sabbato in die saneti Kalixti`{lang=lat} `[October 14]:`{lang=eng}  
`Die kauffleuthe von Collen an deme Reine worden geleitet`{lang=deu}  
`bis kein deme Bunczlaw 4 gr. — Die rothmanne von deme Bunczlaw`{lang=deu}  
`die worden geert mit weyne unde bire 7 gr. — Bartholomeus`{lang=deu}  
`Ebirhard, Mathis Kezer kein der Lobaw zu tage mit landen`{lang=deu}  
`unde steten, als der bisschoff von Olemucz, der von Michels- |`{lang=deu}  
`berg etc., die schepphen von dem Berge?) schrebn, das man en.`{lang=deu}  
`von unsers herrn des koniges wegin durch der keczereie`{lang=deu}  
`— LL`{.error}  
`1) Unflar, -`{lang=deu}  
`2) Kuttenbera. -`{lang=deu}  
