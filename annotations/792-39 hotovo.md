[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/792/39.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=792&page=39

***

`А. 1. Psanj cjsaře Sgmunda r. 1434, 1435. 39`{lang=ces}   
`Janowi Hnidůsowi platy a úroky swými časy wydáwali, a jinák abyste nečinili: pod`{lang=ces}   
`naší milostí; neb sme slowutnym Zdeńkowi Drstkowi purkrabi na Týně a Kolwi-`{lang=ces}   
`nowi jeho bratru poruéili, jestlizebyste poslusni nechtéli byti, jenZ tomu newěříme;`{lang=ces}   
`aby oni wás k tomu drželi a priprawili. Dán w Bazli, léta od narozenie s. Бой.`{lang=ces}   
`tisicieho étyrstého tridcátého étwrtého, tu nedéli po boZiem wstüpenie, let králow-`{lang=ces}   
`stwi našich Uherského oc. XLVIII, Římského w XXIV, Ceského we XIV a ciesař-`{lang=ces}   
`stwie w prwniem létě.`{lang=ces}   
`Ad mandatum D. Imp.": Caspar Slik cancellar. Nico-`{lang=lat}   
`lao de Bladen et Andrea de Studenicz referentibus.`{lang=lat}   
`45.`   
`Panu Oldiichowi z Rosenberka: oznamuge Ze k Cechám se bljzj a posjlá ku pánüm`{lang=ces}   
`českým Půtu z Častolowic i Arnošta z Wlašimě.`{lang=ces}   
`W Ulmu, 1434, 3 Jun.`   
`Sigmund z bożie milosti Římský ciesat wżdy rozmnożitel říše, a Uherský,`{lang=ces}   
`Český oc. král.`{lang=ces}   
`Urozeny wèrnÿ mily! Twym listóm nám nynie poslanym sme dobře sro-`{lang=ces}   
`zuměli, a weseli jsme tiem že sie pánóm dobře wede. A jakožť od nich na nás`{lang=ces}   
`žádáno jest, abychme sie k Čechám pro ty wěci přiblížili: wěz že pro dobré a`{lang=ces}   
`počestné té koruny to rádi učiniti chceme, i wšecko což móžem, jestli jedno`{lang=ces}   
`k čemu. A na to ku pánóm a zwlášče k tobě posieláme urozené wërné nase`{lang=ces}   
`milé Puotu z Castolowic a Arnesta z Wlasimé, kteti zjitra bohdà odsud wyjedü`{lang=ces}   
`upřímo k tobě, a dále ku panôm; a ti tě našeho úmysla úplně zprawie. Dán`{lang=ces}   
`w Ulmu, we čtwrtek před s. Bonifacii, let králowstwi naších Uherského w XLVIII,`{lang=ces}   
`Římského w XXIV, Českého we XIV a ciesařstwie w druhém létě.`{lang=ces}   
`Ad mandatum D. Imperatoris: Caspar Slik Cancellarius.`{lang=lat}   
`Nobili Ulrico de Rosenberg,`{lang=lat}   
`fideli nostro dilecto.`{lang=lat}   
`46.`  
`Témuž zpráwu dáwá: Ze pfigew s králem Bosenskym as knjZaty do Wjdné, tu welice`{lang=ces}   
`zaneprázdněn gest, a protož gednánj s p. Oldřichem až do sgezdu Brnén-`{lang=ces}   
`ského odkládá.`{lang=ces}   
`We Wjdni , 1435, 27 Jan.`{lang=ces}   
`Sigmund z božie milosti Římský ciesař wždy rozmnožitel říše, a Uherský,`{lang=ces}   
`Český oc. král.`{lang=ces}   
`Urozený wěrný milý! Psaní twému o některých běziech, jenž sie nynie`{lang=ces}   
`w Čechách dějí, wděční jsúc i o twých wěcech dobře srozuměwše, wšak u jasného`{lang=ces}   
