[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/53.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=53

***

`10`   
`15`   
`- `{.error} `20`   
`25`   
`80`   
`85`   
`Urfunden sum Jahre 1421. `{lang=deu} `A1`   
`1421. Januar 9.`{lang=deu}   
`Der Görlitzer Rat schreibt an Albrecht von Holzendorf, dass`{lang=deu}   
`er die ilun übergebenen Pfanastiicke am 1. Januar dem Gerhard:`{lang=deu}   
`von der Hosen und seiner Gesellschaft überreicht habe.`{lang=deu}   
`Aus Scultet annal, IL. 54a unter b. 3. 1421,`{lang=deu}   
`Euirs briefes meynunge haben wir wol vernommen, unde`{lang=deu}   
`thun euch zu wissen, das wir euir phande Gherhardo von der`{lang=deu}   
`Hosen und seyner gesellschafft an deme nehsten vergangenen`{lang=deu}   
`tage circumcisionis domini, den man das neuhe jor nennet, ge-`{lang=deu}   
`antwortet haben, noch euir brife laute und usweisunge etc. Sundir`{lang=deu}   
`euch zu liebe willen wir Gherhardo und sinen gesellen schreiben`{lang=deu}   
`und euer bestes in den sachen werben. Und was uns von yn`{lang=deu}   
`zur antwort etc. Gegebin an dem nehsten donerstage noch der`{lang=deu}   
`heiligen dreyer kónige tage, anno etc.`{lang=deu}   
`Dem gestrengen und woltüchtigen rittern, h. Albrechte von`{lang=deu}   
`Holtzendorff, unserm besundern liben frind, a sen. Gorl.`{lang=deu}   
`Das Jahr ergiebt fid) aus dem Umftande, daf im Certe anno etc. {teht und`{lang=deu}   
`das Zeichen `{lang=deu} `„sine anno”`{lang=lat} ` (mit roter Tinte) am Rande fehlt.`{lang=deu}   
`[1421]. März 19. Leipe.`{lang=deu}   
`Hinko Berka von der Duba schreibt den“ Oberlausitzern, dass`{lang=deu}   
`die Hussiten Kommotau eingenommen hiitten und vor Briia`{lang=deu}   
`rückten; sie möchten Hilfe schicken.`{lang=deu}   
`Aus Scultet, annal. IL. 50, Gedruckt bet palady, Urfundl, Beitr. I.`{lang=deu}   
`S. 67. — Regeft bei Zobel, Wberlaufiiiches Urfundenverzeichnif IT.`{lang=deu}   
`S. 5 (unrichtig ins Jahr 1420 gefebt).`{lang=deu}   
`Ich thu euch zu wissen, das die Hussen Commetaw die`{lang=deu}   
`stad und slosz gewonnen haben, und do grossen unglympffen`{lang=deu}   
`gethan haben an mannen, weibern und kyndern, die allzumole`{lang=deu}   
`dyrmord haben. Und haben willen, vorder zu rücken vor die.`{lang=deu}   
`stad Brůx. Vormane ich euch, das ir das wellet thun umb gottes`{lang=deu}   
`willen und och durch des gnedigen herren willen des koniges,`{lang=deu}   
`und off wellit sein mit macht, mit reithenden und mit fuss-`{lang=deu}   
`gengern, das wir den bösen leuthen mögen widersteen, das sie`{lang=deu}   
`unserm gn. herrn seine land also nicht zu schanden machen ete.`{lang=deu}   
`Geben zur Lipen an der grossin mitwoche.`{lang=deu}   
`Hincke Bircke von der Duben zur Leippen gesessen an land`{lang=deu}   
`und stete in obir Lusitz.`{lang=deu}   
`Das Jahr 1421, ergiebt fich aus der Chatfache, daß die Huffiten Kommotau`{lang=deu}   
`r`{.error}   
`am 76. März 1421 einnahmen.`{lang=deu}   
