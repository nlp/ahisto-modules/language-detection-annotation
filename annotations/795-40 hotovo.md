[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/795/40.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=795&page=40

***

`40 C. IX. Nekteré zdpisy`{lang=ces}   
`swrchupsani, to nám jmají sraziti na swych škodách, a tak dlüho mají stawowati,`{lang=ces}   
`zajimati i bráti, tak jakož wýše psáno stojí, dokowadz bychom my swrchupsant`{lang=ces}   
`kněz probošt i wešken konvent i naši budaucí toho wšeho neučinili, což swrchu`{lang=ces}   
`psáno stojí. A kdoZz koliwék tento list bude miti s našich wěřících dobrů wolí a`{lang=ces}   
`dobrým swědomim, ten jmá i jmiti bude wšechna práwa, jakožto oni sami, k tomu`{lang=ces}   
`ke wšemu, což swrchu psáno stojí. À toho wieho na potwrzeni, pewnost i jistotu`{lang=ces}   
`další my kněz Petr probošt i wešken konvent swrchupsaný pečeti naše wlastni`{lang=ces}   
`s naším wedomim i přiznáním, naší dobrů woli napřed k tomuto listu jsme při-`{lang=ces}   
`wösili, a pro další swědomí prosili jsme urozených pánuow i panosi Zdeńka Drstky,`{lang=ces}   
`purkrabí na Týně Horšowském, času toho poručníka od Krälowy Milosti našemu`{lang=ces}   
`klášteru Chotésowskému wydaného i poruéeného, pána pana Hynka z Swamberka,`{lang=ces}   
`Lwika z Jiwian, purkrabí na Přimdě, Jindřicha z Sedlce řečeného Šwajda, purkrabí`{lang=ces}   
`na Radyni, můdrých a opatrných purkmistra a konšelůw města Nowého Plzně,`{lang=ces}   
`že jsú swé peóceti wlastni wedle našich na swódomi k tomuto listu pfiwesili, bez`{lang=ces}   
`jich škody. Jenz jest dán a psán léta od narození syna boziho tisicého étyrstého`{lang=ces}   
`třicátého prwniho, ten pátek pied S. Martinem.`{lang=ces}   
`5,`{lang=ces}   
`Petr Brada z Nekmiře moří zápisy dlužní ztracené, kteréž měl na klášter Chotěšowský.`{lang=ces}   
`1446, 29 Januar.`{lang=lat}   
`Já Petr Brada z Nekmiře, seděním na Poříčí, wyznáwám tímto listem obecne`{lang=ces}   
`přede wšemi, kdož tento list uzří anebo čtůce jej slyšeti budů, že ctihodný kněz`{lang=ces}   
`Wáclaw probošt kláštera Chotěšowského a jeho konvent splnili jsü mi a hotowymi`{lang=ces}   
`penězi zaplatili úplně a docela wšecku summu peněz jistinných a se wšemi ško-`{lang=ces}   
`dami na těch třech listech, jakož sem měl na ctihodného kněze dobré paměti, na`{lang=ces}   
`kněze Hynka a na kněze Petra, probošty kláštera Chotěšowského, a na jich kon-`{lang=ces}   
`vent, kteréžto listy položili sme k wěrné ruce spolu, jménem s knězem Petrem`{lang=ces}   
`dobré paměti s proboštem kláštera Chotěšowského , na hradě na Nečtinách u ne-`{lang=ces}   
`božce Zbyňka z Kocowa; a ty wšechny listy ztraceny a wzaty jsů na hradě na`{lang=ces}   
`Wilšteině po smrti nebožce Zbyňkowě toho času, když jest Swojše hrad Wilštein`{lang=ces}   
`ztekl. Protož jà swrchupsany Petr Brada z Nekmire ty wšechny listy, což sem`{lang=ces}   
`jich kdy jměl, buďto na peníze hotowé anebo na zboží kláštera swrchupsaného`{lang=ces}   
`Chotěšowského, moci listu tohoto kwituji a umořuji, prázdny činím a propauštím`{lang=ces}   
`swrchupsaného kněze Wáclawa probošta Chotěšowského i jeho konvent i jich bu-`{lang=ces}   
`daucí z swrchupsaného dluhu, kterýž sem měl na těch listech swrchupsaných, a`{lang=ces}   
`z toho jich slibuji wěčně nenapominati ani moji budůcí. Toho na swédomi swü`{lang=ces}   
`sem wlastni pecet priwesil k tomuto listu s mů dobrů wolí, a pro lepsi swédomi`{lang=ces}   
`prosil sem urozených pánuow toho času hajtmanůw kraje našeho Plzenského, uro-`{lang=ces}   
