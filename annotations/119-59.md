[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/119/59.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=119&page=59

***

`1422. 35`{.unchecked lang=ces}   
`dester mere gereiczet werden, dorumb mit wolbedachtem mute,`{.unchecked lang=deu}   
`gutem rate und rechter wissen haben wir in dise besunder gnade`{.unchecked lang=deu}   
`getan und tun in die in kraft disz briefs, daz wir sy und die`{.unchecked lang=deu}   
`statt zu Eger mitsampt dem pflegampt und dem gerichte nye-`{.unchecked lang=deu}   
`mand umb keinerley gelt verseczen, verschreiben, noch verpfen-`{.unchecked lang=deu}   
`den, noch sy, weder die stat und pflege mitsampt dem gerichte`{.unchecked lang=deu}   
`von uns und der erone zu Behem empfremden, stossen noch ver-`{.unchecked lang=deu}   
`weisen wollen noch sollen in dheinerley wise. Und ob yemand`{.unchecked lang=deu}   
`queme und mit stetem anligundem bete oder anders von uns brieve`{.unchecked lang=deu}   
`erwurbe, die wider dise unser gnad were, der sollen die egenan-`{.unchecked lang=deu}   
`ten von Eger nicht ufnemen, und sy sollen sich auch nichts do-`{.unchecked lang=deu}   
`ran keren, sunder wir seczen, daz soliche brieve kein kraft noch`{.unchecked lang=deu}   
`macht haben, und die sollen auch den vorgenanten von Eger an`{.unchecked lang=deu}   
`disen unsern gnaden keinen schaden bringen in dheinweisz. Mit`{.unchecked lang=deu}   
`urkund disz briefs versigelt mit unserr kuniglichen maiestat in-`{.unchecked lang=deu}   
`sigel. Geben zu Nüremberg nach Crists geburt virezenhundert iare`{.unchecked lang=deu}   
`und dornach in dem ezweyundezwenezigistem iare am freitag vor`{.unchecked lang=deu}   
`sand Bartholomes tag, unser reiche des Hungrischen ete. in dem`{.unchecked lang=deu}   
`sechsunddrissigistem, des Romisehen in dem ezwelften und des`{.unchecked lang=deu}   
`Behmischen in dem dritten iaren.`{.unchecked lang=deu}   
`[Na ohbu:] &d mandatum domini regis domino G[eorgio],`{.unchecked lang=lat}   
`episeopo Patauiensi, eancellario, referente`{.unchecked lang=lat}   
`Michael, prepositus Boleslauiensis.`{.unchecked lang=lat}   
`[Na rubu:] R. Henrieus Fye.`{.unchecked lang=lat}   
`Orig. perg. v archivu m. Chebu č. 367 s velkou římskou pečetí královskou,`{.unchecked lang=lat}   
`zavěšenou na černožluté šňůře hedvábné. — Na rubu rukami XV. stol. pozname-`{.unchecked lang=lat}   
`náno: »Kunig Sigmunds brief uber die elastewr... Cloostewr uf edel und un-`{.unchecked lang=lat}   
`edel zue legen«, — Gradl, Privilegien der St. Eger str. 22; Siegl, Die Kataloge`{.unchecked lang=lat}   
`des Egerer Stadtarchivs str. 14, č. 367, též 415; Altmann, č. 5007. — V archivu`{.unchecked lang=lat}   
`Chebském chová se také český překlad tohoto listu z XV. stol., v němž se praví:`{.unchecked lang=lat}   
`»A jakož těm svrchu jmenovaným Chebským náš milý bratr král Václav dobré`{.unchecked lang=lat}   
`přál jest, že sú oni mohli a mohú poplatky v našem kraji Chebském poloëiti`{.unchecked lang=lat}   
`vedle jich potřeby, tak často by sě jim zdálo, — tehda jsme prve jmenovaným`{.unchecked lang=lat}   
`purkmistru a radě a měšťanóm obce tej ve Chbě —, milost učinili —, že oni`{.unchecked lang=lat}   
`dále takové poplatky a pomoci v jich krajině Chebské na všech lidech uroze-`{.unchecked lang=lat}   
`ných anebo neurozených a na všech obývajících tej krajiny mohůú položiti a`{.unchecked lang=lat}   
`zaraziti a takovü od nieh bráti a pfijímati a městu ku požitku a k potřebě`{.unchecked lang=lat}   
`obrátiti a poloZiti, tak často by jim toho potfebie bylo, kdyż by sé jim zdálo`{.unchecked lang=lat}   
`beze v&ie odpory a odmlüvánie. A my pfikazujem váem lidem a každému zvlá-`{.unchecked lang=lat}   
