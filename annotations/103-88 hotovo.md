[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/88.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=88

***

`76` ` Urkunden zum Jahre 1422.`{lang=deu}   
`dorch manchirleie sachen wille etc, wart vertrunken 11 gr. —`{lang=deu}   
`Item Hannus Ulrsdorff, Heinrich Otte mit landen unde steten`{lang=deu}   
`kein der Gabel zu tage abir mit den Bemischen herren durch`{lang=deu}   
`unseres herrn des koniges geschofte wille etc. 21% sch.`{lang=deu}   
`5 Sabbato in vigilia Appolonie') [Febr. 7]:`{lang=deu}   
`Mathis Kezer, Johannes Ulrsdorff kein der Lobaw zu`{lang=deu}   
`tage mit landen unde steten durch einer neuen reisen mit den`{lang=deu}   
`Bemischen herren wille kein der Gabil unde also die stete umme`{lang=deu}   
`karten zu deme kônige zu reiten etc. ‚ 28 gr. — `{lang=deu} `Nuncium ad`{lang=lat}   
`10 Lubanum 2 gr. — Item Hannus Ulrsdorff, Heinrich Otte abir`{lang=deu}   
`die woche kein der Lobaw zu tage zu laden die herren von`{lang=deu}   
`Lusicz, lande und stete kein Hoerswerde durch der ketczer`{lang=deu}   
`unde ander geschefte wille unde ouch eine botschaft zu den`{lang=deu}   
`markgrofen von Meissen 99 8r. -— Herzoge Heinrich der elder,`{lang=deu}   
`1 unseres foites bruder, quom zu Uns, wort geert mit wyne unde`{lang=deu}   
`bire 19 gr. — /BI. 97 b] Einen boten zu herzoge Hannus mit einer`{lang=deu}   
`entwort von des anlas?) brife wegen 5 gr. — Her Hannus von`{lang=deu}   
`Muskaw mit seinen frunden wort geert 9 gr. — Her Christoff von`{lang=deu}   
`Ghersdorff mit seinen frunden von Schochaw wart geert etc. 10 gr.`{lang=deu}   
`20 In die saneti Valentini [Februar 14]:`{lang=deu}   
`Heinrich Otte, Heinrich Numan mit landen unde steten`{lang=deu}   
`zu tage kein der Lobaw zu unserem herren zu komen kein`{lang=deu}   
`Olemunez noch seiner brife laute mit andern vil gescheften etc.`{lang=deu}   
`28 gr. — `{lang=deu} `Nuncium ad Lubanum cum copia littere regis`{lang=lat} ` 2 gr. —`{lang=deu}   
`2 Item einen ritenden boten kein der Zitaw bie nacht mit unseres`{lang=deu}   
`des koniges brif und auch das ir statschreiber nicht keym Lem-`{lang=deu}   
`berg reiten derffte, 5 8r. —- /Bl. 984] Nympscher deme kanne-`{lang=deu}   
`gisser vor kannen in die herfart 18 gr.; item vor zen zu den`{lang=deu}   
`buchsen 6 gr. — Vor ein korp of den rynewagen 5. gr. —`{lang=deu}   
`? Kolben, unserem wirte zu Lemberg, von kranker pherde wegen,`{lang=deu}   
`die in den herfarten vorcerunge?) 11 gr.`{lang=deu}   
`[Bl. 98%] `{lang=deu} `In vigilia sancti Petri ad kathedram ante esto mihi`{lang=lat}   
`[Februar 21]:`{lang=deu}   
`Einen boten kein der Swydenicz zu der Swydenicz unde`{lang=deu}   
`5 Zu den fursten, landen unde steten umme hulfe unde rettunge kein`{lang=deu}   
`7) Ueber die Digilie zum Montag, die auf den Sonnabend füllt, f. oben 5. 5`{lang=deu}   
`Unmerf. 3.`{lang=deu}   
`?) anlas = Dergleich, Shiedsurteil.`{lang=deu}   
`?) Kann als eine Art Appofition gefaßt werden.`{lang=deu}   
