[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/792/153.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=792&page=153

***

`C. I. Staré zápisy rodu Šternberskéhc, 1425. 153`{.unchecked lang=pol}   
`kop na wýprawu; a nemohloliby hotowými penězi býti, ale dědinami swobodnými,`{.unchecked lang=ces}   
`jakož země za práwo má, a lo tak aby ujisteno bylo, kdyżby wdáwána byla, aby`{.unchecked lang=ces}   
`na wiece nemohla sahati, než což jest nahoře psáno. Také jest umluweno, abych`{.unchecked lang=ces}   
`já pomocen i raden byl panie Perchtě nahoře psané, jestližeby pan Smil bratr`{.unchecked lang=ces}   
`mój chtěl na paní neboli na to zbozie sáhnüti. Pakliby pan Smil tak daleko ty`{.unchecked lang=ces}   
`hrady neb to zbozie swrchupsané na panie Perchté iádem a práwem zemskym`{.unchecked lang=ces}   
`dosáhl, dobyl aneb wysúdil, tehdy já Aleš nahoře psaný hned z těchto umluw`{.unchecked lang=ces}   
`pani Perchtu jmam propustiti. Take jest umluweno, což jest pani nynie dluhuow`{.unchecked lang=ces}   
`dluzna, aby mi ty jisté dala napsati. A jestlizeby se panie Perchta pro sirotéi`{.unchecked lang=ces}   
`dobré po téchto umluwách musila zdlużiti, aby mně to dala popsáno, weč by se`{.unchecked lang=ces}   
`zdlužila, a já jí pomocen mim byti, jakożto piietel a strýc těch sirotkuow. A také`{.unchecked lang=ces}   
`toto jest umluweno: jestližeby který purkrabie byl panie kázáním po těchto umlu-`{.unchecked lang=ces}   
`wách ssazen, tehdy ten jiný, kterýž na jeho miestě by byl, aby mi slíbil tímž bě-`{.unchecked lang=ces}   
`hem, jako prwni, tolikrát, kolikrát by toho potřebie bylo.`{.unchecked lang=ces}   
`A tyto jsů se umluwy staly w Krumlowč, páně Rosenberského zámku, a`{.unchecked lang=ces}   
`na jeho hradě, kteréžto umluwy já Aleš slibuji pode ctí a pod swu dobru wóri`{.unchecked lang=ces}   
`křesťansků üplné drzeti a zachowati, tak jakož jest swrchu psáno.`{.unchecked lang=ces}   
`Toho wšeho na potwrzenie swů jsem pečet přiwěsil, a pro lepsi pewnost`{.unchecked lang=ces}   
`prosil jsem urozeného pana Oldřicha z Rosenberka, pana Jana z Risenburka, Ma-`{.unchecked lang=ces}   
`téje Wisni w tu chwili purkrabie na Welešíně, Buška z Rowného, w tu chwili`{.unchecked lang=ces}   
`purkrabie na Krumlowe, Karla z Cehnic a Alse z Céhoste, kteříž jsů k mé prosbě`{.unchecked lang=ces}   
`wècem nahoře psaným swé peceti na swédomie piiwesili k tomuto listu. Jenž jest`{.unchecked lang=ces}   
`dàn léta od narozenie syna bozieho tisícého čtyřstého étyrmecietmého, ten úterý`{.unchecked lang=ces}   
`před sw Klementem.`{.unchecked lang=ces}   
`17.`{.unchecked lang=ces}   
`Albrechta ze Šternberka a z Laukowa kompromiss we při se strýci swými o Laukow`{.unchecked lang=ces}   
`a Holešow.`{.unchecked lang=ces}   
`Na Helfensteint, 8 Mai 1425.`{.unchecked lang=ces}   
`Ja Albrecht ze Šternberka, odjinad z Lůkowa, wyznáwám tiemto listem`{.unchecked lang=eng}   
`sbecně předewšemu, ktoż jej uziie nebo étuc slyšeli budú, že o ty wšechny ne-`{.unchecked lang=eng}   
`zhuti a zawady, kteréž sů byly mezi mnů a urozenými pány Jiříkem a Lackem`{.unchecked lang=eng}   
`brattiemi ze Šternberka a z Lůkowa, strýci mými, o puol hradu Lükowa a o twrz`{.unchecked lang=eng}   
`Holesow, i o ta zboZzie a panstwie, coZ k tomu piislusie, i o jiná wsechna pan-`{.unchecked lang=eng}   
`stwie a listy, což jest mezi mými a jich předky zašlo, přišel sem: a mocí tohoto`{.unchecked lang=eng}   
`listu mocně přicháziem na urozeného pana Petra z Krawař a ze Strážnice, jakžto`{.unchecked lang=eng}   
`na mocného smluwčieho a ubrmana, a slibuji pod swü wérü a pod swů ctí, pod`{.unchecked lang=ces}   
`20*`{.unchecked lang=ces}   
