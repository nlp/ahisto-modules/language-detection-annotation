[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/350/163.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=350&page=163

***

`144 208—210: 31. bfezna — 1. kvétna 1482.`{.unchecked lang=ces}   
`208. V Krumlově, 31. března 1432.`{.unchecked lang=lat}   
`Oldřich z Rožmberka a jiní svědčí, že Bušek Holkovec z Holkova postoupil`{.unchecked lang=ces}   
`všecko své zboží své ženě Kateřině z Dobeve.`{.unchecked lang=ces}   
`Treboñ, Schwarzenb. archiv: Fam. Holkovec von Holkow è. 2, or. perg.`{.unchecked lang=fra}   
`Sehmidt-Picha, UB. d. St. Krummau in B. II, 22 č. 83 (reg).`{.unchecked lang=fra}   
`My Oldřich z Rosenberka, Jindřich z Vartenberka, Jan z Risenburka, Matěj Višně`{.unchecked lang=ces}   
`z Větřnie, Buzek z Rovného a Mrakeš z Radimovic vyznáváme tiemto listem všem,`{.unchecked lang=ces}   
`ktož jej čísti nebo čtůce slyšeti budú, že slovutný panoše Bušek Holkovec z Holkova`{.unchecked lang=ces}   
`před nás přišel a dobrovolně vyznal, že to všecko, což má v Vesci a v Dobevi i jinde,`{.unchecked lang=ces}   
`kdež co má neb mieti bude, a tak jakož sám držal, mocně jest vzdal, postůpil, vzdává`{.unchecked lang=ces}   
`i postupuje paní Katheřině z Dobeve, své ženě, tak aby ona po jeho smrti s fiem`{.unchecked lang=ces}   
`se vším mohla, což by sě jie zdálo, jako sjsvým vlastním učiniti. Kteréžto vyznánie`{.unchecked lang=ces}   
`před námi proto sě stalo, neb jest v tu chvili pro bufi a pro nepokoj v zemi ke dekám`{.unchecked lang=ces}   
`přístup nemohl býti; než když by dcky otevřeny byly a k nim přístup mohl býti,`{.unchecked lang=ces}   
`że jest svrchupsany Bušek to vyznánie a vzdánie nahořepsané slíbil ženě své dekami`{.unchecked lang=ces}   
`učiniti a před úředníky podle běhu země této, ač jestli Ze by jeho buoh neuchoval.`{.unchecked lang=ces}   
`Pakli by jeho buoh v té mieře neuchoval, tehdy aby toto vyznánie tak mocno bylo,`{.unchecked lang=ces}   
`jakoż by sě dekami stalo před úředníky. Tomu na svědomie své jsme pečeti s naším`{.unchecked lang=ces}   
`dobrým vědomím přivěsili k tomuto listu, Jenž jest dán v Crumlově léta od na-`{.unchecked lang=ces}   
`rozenie syna božieho tisícieho čtrstého třidcátého druhého, ten pondělí ve středopostí.`{.unchecked lang=ces}   
`Na perg. proužcích přivěšeno šest pečetí: 1) velká pecet Oldficha z Rožmberka, popis viz u č. 8.`{.unchecked lang=ces}   
`2) z červeného vosku, štít rozpůlený, nad helmou kfidlo, napis: + S + henrici + de + wartenberk`{.unchecked lang=ces}   
`a) pečeť Jana z Risenburka, popis viz u č. 102, 4) pečeť Matěje Višně z Větřní, popis viz uč. 102.`{.unchecked lang=ces}   
`5) Buzka z Rovného, popis víz u č. 57. ó) z černého vosku, na štítě tři příčná břevna, nad helmou`{.unchecked lang=ces}   
`rozkřídlená husa, nápis: :mrakes-z-radimovic:`{.unchecked lang=ces}   
`209. V Linci, 28. dubna 1432.`{.unchecked lang=ces}   
`Oldřich z Rožmberka źddd Cdhlavskć, aby dovolili dovézti mu bez překážky`{.unchecked lang=ces}   
`do Krumlova 74 becky soli. — (mantag nach sand Jorigen tag).`{.unchecked lang=ces}   
`Wirmsberger, Regesten aus dem Archive Freistadt in Österreich ob d. E., Archiv f. Kunde`{.unchecked lang=pol}   
`dsterr. Geschichtsquellen XXXI (1864), 316 (reg. 2 ném. orig.). — Schmidt-Picha, UB. d. St. Krummau`{.unchecked lang=pol}   
`in B. I], 22 č. 84 (reg).`{.unchecked lang=pol}   
`210. Ve Vídní, 1. května 1432.`{.unchecked lang=lat}   
`| Oldřich z Rožmberka slibuje, že zaplatí po dvou letech do měsíce svůj dluh`{.unchecked lang=ces}   
`4.000 zl. uherských Fridrichovi Rakouskému, bude-li jím upomenut; zaplatí-li, bude`{.unchecked lang=ces}   
