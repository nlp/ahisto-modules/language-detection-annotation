[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/913/23.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=913&page=23

***

`disen briff gehangen der do gegeben ist czu Obirste Glogaw noch Christigeburt ffirczen-`{lang=deu}   
`hundirt Jore, dornoch yn dem fiervnddreisigsten Jore an dem tage des hochgelobten czwelfz`{lang=deu}   
`boten Sancti Thome.`{lang=deu}   
`Bolko, Fürst von Oppeln und Herr zu Ober Glogau, bestätigt Hanus, dem Mühler bei dem`{lang=deu}   
`Hrabower Tore in Ostrau einen Brief. Ober Glogau, den 21. Dezember 1434.`{lang=deu}   
`Pergamenový originál v městském archivu s visutou pečetí, na níž je slezská orlice s opisem málo čitelným:`{lang=ces} `Sigillu. Bolcon.`{lang=lat}   
`Rozměr listu 31'5 cm X 18'5 cm; plica: 44 cm. Na rubu listu:`{lang=ces} `„Begabnuß auf die rechte Mihl beij dem Grabowerthor stehend.`{lang=deu}   
`No 17.“`{lang=deu} `a potom`{lang=ces} `„No 20°.`{lang=deu}   
`14. 1457, únor 25. Praha.`{lang=ces}   
`Zikmund, král český, zastavuje panství hukvaldské Mikuláši Sokolovi z Lamberka.`{lang=ces}   
`My Sigmund z božie milosti římský ciesař vždy rozmnožitel říše a uherský, český,`{lang=ces}   
`dalmatský, charvatský etc. král vyznáváme tiemto listem obecně přede všemi, kdož jej`{lang=ces}   
`uzřie anebo étüce slyseti budü, že jakož před lety některými bylisme s povolením biskupa`{lang=ces}   
`našeho olomúckého a jeho Capitoly zastavili hrady Hukvaldy s tiem, což k tomu příslušie,`{lang=ces}   
`osviecenému Bolkovi kniežeti opolskému, ujci našemu milému, aby je i sbožie k nim príz`{lang=ces}   
`slušná držal a bez škody té země i jiných všech okolních zemí jich požíval, kteréžto`{lang=ces}   
`hrady pod chvílemi ztratil jest těm a takovým lidem, ježto skrze to a odtud škody`{lang=ces}   
`mnohé a veliké i záhuby nekřesťanské té zemi i jiným vuokol příležícím zemiem sú se`{lang=ces}   
`diály, pak ti hradové i s příslušentsvím svým skrze urozeného Jana z Cimburka jinak z To:`{lang=ces}   
`vačova konečně v moc a držení urozeného Mikuláše Sokola z Lamberka, věrného našeho`{lang=ces}   
`milého pravým kupem sú přišly. A tak my o to jinak s týmž Sokolem uhoditi nemohúce,`{lang=ces}   
`ty hrady s vesnicemi dole psanymi i s jich příslušentsvím a k tomu sbožie jeho sbozie`{lang=ces}   
`Šaušteinské s vesnicemi, dvory, lesy, horami i se vším, což k tomu přislušie, pravým`{lang=ces}   
`trhem u něho kúpili sme za tři tisíce kop grošuov pražských stříbrných a dobrých. Protož`{lang=ces}   
`s dobrým rozmyslem a radú naší jemu Sokolovi a jeho dědicom i budúcím to obé spolu,`{lang=ces}   
`to jest hrady Hukvaldy, Ostravu, město Příbor a Brunšperk, městečka s vesnicemi Staříče,`{lang=ces}   
`Fričovice, Petrovice, Skotnice, Chlebovice, Vítkovice, Palkovice, Motylovice, Kozlovice, Děts`{lang=ces}   
`řichovice, Mniše, Sklenov Veliký, Hnojová Lhota, Tichá, Klokočov a Sklenov Malý s many,`{lang=ces}   
`dvory, poplužím, úroky, poplatky, lesy, lukami, rybníky, zahradami a vedle toho sbožie Sau-`{lang=ces}   
`šteinské, Frankštát městečko a vsi Lichtnov, Veličovice, Drnholec, Copřivnice a půl Závišic`{lang=ces}   
`se dvory také lesy, horami, rybníky, platy, užitky i konečně se vším což k Hukvaldom`{lang=ces}   
`neboli k tomu sbožie Šaušteinskému z pradávna spravedlivě příslušie, kterýmby to neb`{lang=ces}   
`kterakymkoli jménem bylo neb mohlo jmenováno býti a odkudkoli neb z čehožkoli pos`{lang=ces}   
`jiti nic ovšem nepozůstavujíc i s cielým a plným panstvím zastavili jsme- a zastavujem,`{lang=ces}   
`zapsalisme a tímto listem zapisujem v těch všech penězích, za kteréž sme od něho kú`{lang=ces}   
`pili, to jest ve třech tisících kopách grošuov pražských striebrných a dobrých téhož jakož`{lang=ces}   
`svrchu se píše rázu a to tak, aby prve pravený Sokol i jeho dědicové a buduci ta`{lang=ces}   
`sbožie s jich příslušnostmi jakož již vypsána jsú v těch penězích držali a jich požívali`{lang=ces}   
`bez našie, našich budúcích králuov českých a jiných všech každé otpory a překážky`{lang=ces}   
`v úmluvu takovúto, kdyžbychom koli my, naši budúcí králové čeští neboli ti, jimž by ta`{lang=ces}   
`výplata spravedlivě příslušala svrchu psanému Sokolovi nebo jeho dédicom a budûcim`{lang=ces}   
`23`   
