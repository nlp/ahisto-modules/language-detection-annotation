[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/818/425.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=818&page=425

***

`Pabérky písemností wšelikých s leś 1421—1438. 425`{lang=ces}   
`29.`
`Přemek kníže Opawský wstupuje w příměří s wojskem Táborským a Sirotčím a slibuje při-`{lang=ces}   
`staupiti ke 4 článkům Pražským.`{lang=ces}   
`W Opawé, 28 Sept. 1431 (Z rkp. K¥idownického.)`{lang=ces}   
`My Přemek z božie milosti knieže a pán Opawský, wyznáwáme tiemto listem`{lang=ces}   
`předewšemi, ktož jej widěti, čísti aneb slyšeti budú, že sme smlúwu a dokonalü`{lang=ces}   
`umluwu učinili s statečnými bojowníky o prawdy zákona božieho, s Otíkem z Lozy,`{lang=ces}   
`Janem Čapkem z Sán haitmany, s knězem Prokopem, Táborem polním i se wšemi`{lang=ces}   
`staršími wojsk Táborských polniech a Sirotčiech ustawičně polem pracujícími, takowúto :`{lang=ces}   
`Najprwé w příměří křesťanské s nadepsanými haitmany i s jich budúcími`{lang=ces}   
`náměstky i se wšemi staršími a obcemi wšemi k zákonu božiemu příchylnými, od`{lang=ces}   
`dání tohoto listu až do celého plného roku sme wsthpili.  Kteréžto příměří se wšemi`{lang=ces}   
`našimi zemany, městy i se wšemi lidmi w zemi naší nám poddanými slibujem swr-`{lang=ces}   
`chupsaným haitmanuom i jich budúcím náměstkuom i starším wojsk nadepsaných ctně`{lang=ces}   
`a křesťansky držeti a zachowati beze wší zlé lsti a přerušenie ; toto znamenitě wy-`{lang=ces}   
`minujíce: kdyžby nadepsaná wojska táhla skrze kniežetstwie naše, cožby k potřebám,`{lang=ces}   
`k jiedlu a k picowánie wzali, že to příměří škodno býti nemá.`{lang=ces}   
`A když se ten rok příměřie tohoto dokoná: tehdy ihned po najposlednějším`{lang=ces}   
`dni wyjetí příměřie swrchupsaného slibujem my kněz Přemek s knězem synem swým`{lang=ces}   
`i s jinými syny swými k prawdám božím přistúpiti, zwláště k tělu a ku krwi boží`{lang=ces}   
`a k těm čtyřem kusóm, o něž sú se obce swrchupsané zasadily; a ty slibujeme`{lang=ces}   
`a mime drZeti. Paklibychom toho neučinili a nedokonali, čehož bože ostřez: tehda`{lang=ces}   
`sme swrchupsaným haitmanuom a obcem anebo jejich náměstkuom propadli čtyři`{lang=ces}   
`tisíce kop grošów dobrých stříbrných rázu českého a čísla morawskóho, za kazdü`{lang=ces}   
`Корё Lx grošuow počítajíce; kteréžto penieze slibujeme a máme po propadenie dne`{lang=ces}   
`prwnieho we dwü mésící w Praze w starém městě na rathús w moc haitmanuow`{lang=ces}   
`a obci nadepsanych anebo jejich náměstkuow poloziti A kdyZbychme ptistüpili,`{lang=ces}   
`tehda slibujeme proti každému, ktožby anebo kteříž chtólby anebe chtěli ty prawdy`{lang=ces}   
`boží tupiti, skutečně pomocni byti. Jestli pak žeby nás kněze Přemka pán buoh`{lang=ces}   
`w tomto příměřie neuchowal, čehož bože ostřez: to tomuto našemu slibu nemá`{lang=ces}   
`škodno býti, ale wždy ti slawní čtyři kusowé mají kázáni býti a skutečně wedeni`{lang=ces}   
`od našich synuow zuostalých, a nemá proto kněžiem wěrným a lidem překáženo býti.`{lang=ces}   
`A my Jan z Krawař, pán Jičinský, Jan z Cimburka a z Towačowa, Zibřid`{lang=ces}   
`z Bobolusk, Wáclaw z Kobeřic, rukojmě za swrchupsané knieže, kněze Přemka i za`{lang=ces}   
`jeho syny, přiznáwáme se tiemto listem, že sme slíbili i tiemto listem slibujem, pod`{lang=ces}   
`54`
