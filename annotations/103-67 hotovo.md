[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/67.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=67

***

`10`   
`15`   
`20`   
`25`   
`80`   
`35`   
`40`   
`Urfunden zum Jahre 1421.`{lang=deu} ` |`{.error} ` 55`   
`Aus Górliker Ratsardhiv, Copie aus dem Anfang des 16. Jahrh. auf`{lang=deu}   
`Pergament — Auch bei Scultet. annal. П. 56Ъ, 57а, — offer,`{lang=deu}   
`Oberlauf, Merfiroiafeiten I. S. mo Anm. Palacky, Urfundlihe`{lang=deu}   
`Beiträge I. S. 140. Oberlauf. Nachlefe 1773 S. 267. Oberlauf. Urfunden-`{lang=deu}   
`abfchr. V. BL. 36 n. 742. Sauf. Magazin 1774 S. 153 f., 382.`{lang=deu}   
`Wir Sigemund von gots gnaden Rómischer kónig, zu allen`{lang=deu}   
`zeiten merer des reichs, und zu Hungern zu Behem etc. konig,`{lang=deu}   
`bekennen und thun kundt offentlich mit diesem brive allen, die`{lang=deu}   
`in sehen adder hóren lesen: Wiewol das ist, das die burger-`{lang=deu}   
`meister, ratman und burgere gemeinlich der Stad Budissin,`{lang=deu}   
`Gorlicz, Sittau, Lubau, Luban, Camentz und anderer stete zu in`{lang=deu}   
`gehórig, unsere lieben getreuhen, durch gotis und des christen`{lang=deu}   
`glauben willen widder die ketzer itzund uber die gemergke und`{lang=deu}   
`grentz uns gedinet haben, und noch dinen wollen und sollen,`{lang=deu}   
`jedoch so meynen und setzen wir von koniglicher macht zu`{lang=deu}   
`Behem, das in das in künftigen zeiten ahn iren rechten, frey-`{lang=deu}   
`heiten, gnaden und briffen, die sie vormols von unsern vorfahrn,`{lang=deu}   
`konigen und fürsten, redlich herbracht und erworwen haben,`{lang=deu}   
`-keynen schaden brengen noch fugen soll in keyne weis. Mit`{lang=deu}   
`urkund dis briffes, vorsigelt mit unserm königlichen anhangenden`{lang=deu}   
`insigel, geben zu Presburg an sandt Bragseden tage, noch`{lang=deu}   
`Christus geburt vierzehenhundert und dornoch in dem 21. jare,`{lang=deu}   
`‚unser reiche des Hungerischen in dem 35., des Rómischen in dem`{lang=deu}   
`ellfften, und des Behemischen in dem ersten jaren.`{lang=deu}   
`[Auf dem Umbug./`{lang=deu} ` |`{.error} ` Ad mandatum domini regis`{lang=lat}   
`|`{.error} ` Franciscus prepositus Boleslaviensis.`{lang=lat}   
`Un demfelben Tage erhält die Mannfdhaft der Oberlanfik einen gletchlauntenden`{lang=deu}   
`Revers, {. Redern, Lusat. super. diplom. 1724 S. 32 (falfche Daten und`{lang=deu}   
`Datierung); danach Oberlauf. ColleFtionswert I. S. 1026. Oberlauf. Urkunden:`{lang=deu}   
`verzeichnis II, S. 3f. Oberlauf. Urfundenabfdr. V. BL 35 n. 74,`{lang=deu}   
`1421. Juli 22. Bautzen. i`{lang=deu}   
`Der Landvogt Heinrich, Herzog von Glogau, vereinbart im`{lang=deu}   
`|`{.error} ` Namen der Sechsstädte mit den Markgrafen von Meissen ein`{lang=deu}   
`5jähriges Schutz- und Friedensbündnis.`{lang=deu}   
`Aus Palacky, Urfundl, Beitrdge I. S. 141, nach dem Original im Staats-`{lang=deu}   
`archiv zu Dresden mit drei anhängenden Infiegeln. Unf der Milichichen`{lang=deu}   
`Bibliothef mspt. fol, 230 S. 64 eine ,usschrift des bundes mit den`{lang=deu}   
`Meissenern anno etc. 21”, danad) in den Oberlanf. Urfundenabfdrift. V.`{lang=deu}   
`BL. 47, Oberlanf. Urfundenverseidn. IL. S. 6 extr. Erwdhnt bei Groffer,`{lang=deu}   
`Kauf. Merkwürdigkeiten I. 5. 110.`{lang=deu}   
`Wir Heinrich genand Rumpold, von gotes gnaden hertzog`{lang=deu}   
`und herre zu Grosenglogaw, foyt zu; Budissin, Gorlicz, Sittaw,`{lang=deu}   
