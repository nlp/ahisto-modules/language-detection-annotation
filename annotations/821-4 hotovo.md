[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/821/4.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=821&page=4

***

`Dopisy 2 lel. 1411—14930. 8`{lang=ces}   
`švagra mého milého: i slyším s pravou věrou rád její dobré i počestné. A také chci`{lang=ces}   
`rád a pilně obeslati pana Michalce a jeho upomínati vedle toho, jakož mi jest svě-`{lang=ces}   
`feno o[d] mateře nebošce její, abych já jeho upomínal. Datum feria secunda ante`{lang=ces}   
`purificationis beatae virginis Mariae annorum 1412.`{lang=ces}   
`1477.`   
`Elška z Koldic Jindřichovi z Rožmberka: o svém zasnoubení za Mikuláše Zajíce z Hazmburka`{lang=ces}   
`a svém věnu, zapůjčeném panu Michalcovi. (M)`{lang=ces}   
`Na Budyni (1412, v březnu?) — Opis Březanův v genealogii pánů z Koldic v arch. Třeboň.`{lang=ces}   
`Modlitba má ustavičná TMti, urozený pane a ujče můj milý! By zdráv byl`{lang=ces}   
`a milý pán Bůh TMti štěstí a vše dobré rozmnožil, to bych srdečně ráda TMti,`{lang=ces}   
`milý pane, vždycky viděla i slyšela. A rač věděti, urozený pane a ujče milý, že mne`{lang=ces}   
`přátelé boží volí urozenému pánu, panu Mikulášovi Zajícovi, pánu na Budyni, v man-`{lang=ces}   
`Zelství zjednali; a to jest bohdá mé počestné a dobré. I prosím TMti, milostivý`{lang=ces}   
`pane a ujče milý, račiž mi milostivě nápomocen býti těch peněz mých ode pana`{lang=ces}   
`Michalce, kterých jest svěřeno TMti a k mé ruce, neb mi jich jest již potřebí k mému`{lang=ces}   
`dobrému. A rač jej z nich pilně a snažně upomínati, neb milý pane a ujče milý,`{lang=ces}   
`nebude-li spomoci pilné a upřímné tvé k tomu, tehdy se bojím, žeť mi ty peníze`{lang=ces}   
`a statček velmi nesnadně od něho vyjde. I ufäm TMti, že TMt milostivě ke mně`{lang=ces}   
`se ukáže, a dopomůžeš mi toho milostivě k mému dobrému a počestnému. A z toho`{lang=ces}   
`chci za TMt ráda milýho pána Boha snažně a věrně, dokud sem živa, prositi. Od-`{lang=ces}   
`povědi prosím milostivé. Dán na Budyni.`{lang=ces}   
`Elška z Koldic, dcera nebošky paní Klářina z Michalovic,`{lang=ces}   
`ujčina tvá, paní na Budyni, modlitebnice tvá.`{lang=ces}   
`Urozenému pánu, panu Jindřichovi z Roznberka, pánu na Krumlově, pánu a ujci`{lang=ces}   
`mému milému, buď list dán.`{lang=ces}   
`Na to od pana Jindřicha z Rožmberka odpověď (v témže smyslu, však nečitelná).`{lang=ces}   
`1478.`
`Farář Suchdolský Oldřichovi z Rožmberka: stěžuje si na skracování svých příjmů, na nekře-`{lang=ces}   
`stanské živobytí některých osadníků a p. (M)`{lang=ces}   
`B. m. a d. (1430?) — Současný opis arch. Třeb. I. A. 3 Ke, 63 h.`{lang=ces}   
`Já farář z Suchdola, kaplan váš, dávám TMti na vědomie, urozený a milostivý`{lang=ces}   
`pane milý, najprve, že Jan Štítenský purkrabě praví, bych neměl pokrčmného mieti`{lang=ces}   
`v Suchdole, jakož jsem od staradávna ten úrok bral, jakož sem sě v kostel uvázal,`{lang=ces}   
`a toho jest dvojieho XXIII groše do roka. Tuť jest mi Jan rychtář z Třeboně od-`{lang=ces}   
`jal jeden úrok XII gr. a Štítenský purkrabě druhý také XII gr. Také žaluji VMti`{lang=ces}   
`na rybáře z Suchdola na Kříhu a na Fileše, žeť mi nechtie dávati desátku rybného,`{lang=ces}   
`1*` 
