[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/70.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=70

***

`58` ` Urfunden zum Jahre 1421.`{lang=deu}   
`[Bl. 59b] Item nota was off die herfart ist gegangen eto.:`{lang=deu}   
`Urban deme moler von den schirmen und setzetarschen!)`{lang=deu}   
`uss zu richten obiral, item deme selben von 20 poffensen*)`{lang=deu}   
`zu molen unde uss zurichten, item vor lymut derzu 21 gr.; vor`{lang=deu}   
`5 geceynte fegemulden*) den pferden in den marstall 8 gr.; vor`{lang=deu}   
`schusseln 10 gr.; vor kannen und toppen 10 gr.; deme hammer-`{lang=deu}   
`meistir zum Penzk vor vir dryfuss zu der kochen !/s mr.; vor`{lang=deu}   
`leffl 7 gr.; vor lymut zu tischlachen unde hantuch 18 gr.;`{lang=deu}   
`vor exse; hauen zu roden unde keilhauen 27 gr.; vor schauffeln`{lang=deu}   
`10 6 gr. item 11 gr.; vor senchsen*); vor sicheln; vor brot; vor`{lang=deu}   
`fleisch rinderinne 44 gr.; vor fische 2 mr.; worze 1 sch. 16 gr.;`{lang=deu}   
`vor salz 18 gr.; vor smalz; vor seitenfleisch 6 sch.; vor speck-`{lang=deu}   
`fleisch Mates Kezer 1 mr; vor erweis?) !/h mr.; vor grys 14 gr.;`{lang=deu}   
`vor grutze; vor grupen, gerste etc. 14 gr.; vor cleyne keze`{lang=deu}   
`15 1 sch. 18 gr.; item vor heringe !/s sch.; — / BI. 60a] den rynner[n]`{lang=deu}   
`in die herfart vor weis unde rot gewant; item vor scherlon dovone`{lang=deu}   
`8 gr.; item den sneidern zu machelone; item den wepenern in`{lang=deu}   
`die herfart idermanne 1 sch.; item den schutzen in die herfart;`{lang=deu}   
`deme pulvermacher 3 sch., 4 sch., item 2 sch., 2 sch., item`{lang=deu}   
`20 3 sch., item 2 sch.; Grumt deme furmanne, den man of der herfart`{lang=deu}   
`reise mit acht pferden holden muste, 1 sch. 17 gr.; item Gunczel`{lang=deu}   
`einem furmanne 1 mr. 4 gr.; von kripptuchen zu machen 9 gr.; deme`{lang=deu}   
`buchsenmeister vor unslit 7 gr.; item vor lichte 2 gr.; Numan`{lang=deu}   
`. deme smede vor 12 sch. gelote ysen zu den buchsen 2!/s sch.,`{lang=deu}   
`2% item lade isen zu den buchsen 54, facit 7 fert. 2 gr.; vor erich*)`{lang=deu}   
`zu pulversecken unde zu mache lone; item den buchsenmeistern`{lang=deu}   
`vor ringe, land”), kernisen®), roste, kellen etc. zu den buchsen`{lang=deu}   
`unde formen 54 gr.; vor stricke zu krippentuchen 32 gr.; vor`{lang=deu}   
`1) Bretterne Wand zum Schuße des Biichfenmeifters, |. S. 34, Unmert 15.`{lang=deu}   
`2) Grofer Sekefdild {. oben S. 36, Unmerf. 5.`{lang=deu}   
`3) Derzinnte Mulden zum Getretderetnigen.`{lang=deu}   
`4) Bei manchen Poften ift der Preis meagelaffen; vielfach jedoch ift es möglich,`{lang=deu}   
`daß der betreffende Gegenftand zufammen mit oem folgenden eine Preisbeftimmung`{lang=deu}   
`hat. Weil das nun nicht überall entfchieden werben Fann, fo iff mam aud) über die`{lang=deu}   
`&Geidyenfeuna (ob Semifolon ober Momma) fehr häufig [dwanfeno.`{lang=deu}   
`5) Erbfen.`{lang=deu}   
`6) Weiß gegerbtes Leder.`{lang=deu}   
`fe © T) ^ft wohl eine HBolzunterlage, auf weldhe Blet genagelt it, {. Schiller п. $£übben`{lang=deu}   
`mittelniederdeutiches Serifon IT. S. 621.`{lang=deu}` | | |`{.error}   
`8) Dielleicht = beftes Eifen. |`{lang=deu}   
