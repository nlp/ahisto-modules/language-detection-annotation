[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/18.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=18

***

`6` `Urfunden jum Jahre 1419.`{lang=deu}   
`Awer von der Neysse wart geeret, als her von Florencz quam`{lang=deu}   
`7 gr.; item wart her us der herberge gelost 9 gr. — Item als`{lang=deu}   
`die unsern mittenander von deme Goltberge wider quomen unde`{lang=deu}   
`ein auch zu Legeniez woren!), do ossen abir mittenander zu deme`{lang=deu}   
`5 burgermeister 2 sch.; item do worden die stete abir eyn mol`{lang=deu}   
`geeret 18 gr. — Nielos Wyder, Mathes Kezer kein der Zitaw`{lang=deu}   
`mit den steten, als die von der Zitaw mit Spengeholeze orteil`{lang=deu}   
`felletin unde keigen Lutenbricz?) santen cum curru 1 sch. minus`{lang=deu}   
`3 gr. — [Bl. 283a] Einem boten, den Peuschel von deme Golt-`{lang=deu}   
`10 berge her heym sante von eyns pherdes wegin, 5 gr. — Item`{lang=deu}   
`deme wirte unde smede zu deme Goltberge von sichen pherde,`{lang=deu}   
`die an der reise hinderstelhg bleben, 3 fert. 3 gr.`{lang=deu}   
`[ №. У. Bl. 2a] [September 30?)]:`{lang=deu}   
`Mathis Kezer, Caspar Lelaw kein der Zitaw mit den steten`{lang=deu}   
`15 zu tage, als der Zitawer urteil mit Peter Spengehoulcze von`{lang=deu}   
`Leutenbricz komen woren 31 gr. — Hasse der stat diner kein`{lang=deu}   
`Budessin zu ern Wende von Ileburg mit lande unde stete brife`{lang=deu}   
`usschrifte, die her vorsiegeln solde, den fürsten zu Polan zu`{lang=deu}   
`senden 8 gr. — Dornoch sante man einen lauffenden zu den`{lang=deu}   
`?0 fürsten zu Polan, Saga, Crossen, Glogaw, Gruneberg mit den`{lang=deu}   
`selben brifen 14 gr. — Her Jacobus Baruth unde die ratmanne`{lang=deu}   
`von Budessin worden geert mit wyne unde bire 10 gr. — Her`{lang=deu}   
`Jacobus Baruth nam mit im unser brife kein Florencz zu unserm`{lang=deu}   
`sollicitatorl unde wart geleite[t] kein deme Bunczlaw 4 gr.`{lang=deu}   
`1) Heißt wohl: Einige auch zu Liegnily gewefen waren.`{lang=deu}   
`2) $eitmertb.`{lang=deu}   
`3) Die Überfchrift diefer Wochenrechnungen Гашек: Sabbato in die sancti`{lang=deu}   
`Marci evangeliste, danad) würden die Eintragungen auf Sonnabend den 25. April`{lang=deu}   
`fallen und da von den hier in Betracht fommenden Jahren nur 1422 den 25. April auf`{lang=deu}   
`einem Sonnabend liegen hat, fo Fönnte man vielleicht daran denken, die vorliegenden`{lang=deu}   
`Ausgaben auf diefen Tag zu fegen. Das ijt aber unmöglich, denn unter dem 25. April`{lang=deu}   
`1422 (in vigilia misericordias domini) find bie Rechnungen vorhanden und fie`{lang=deu}   
`ftimmen ganz und gar nicht mit den unfrigen überein (f. unten). Хип berichten die in`{lang=deu}   
`Rede ftehenden Notizen von der Anmefenheit des Meno von Eilenburg in der Ober-`{lang=deu}   
`lauft, ferner fagen fie, daß ein von Zittau ans nach $ecitmeri gefandtes Urteil nach`{lang=deu}   
`Sittan 3urüdge[didt fei — daffelbe war aber in der Woche nach dem 23. September`{lang=deu}   
`1419 (f. oben) dorthin abgefchiekt. Beide Umftände zwingen uns, die Rechnungen etwa um`{lang=deu}   
`oen 1. Oftober 1419 3u legen. Mun fehlen unter den Wochenrechnungen des J. 1419`{lang=deu}   
`diejenigen, die auf den 30. September fallen. Nach alle dem fann man als fiber ar`{lang=deu}   
`nehmen, daß diefelben hier vor uns liegen, wie denn auch die Stelle, wo fie fid) in`{lang=deu}   
`mitten der andern Rechnungen finden, darauf hinweift.`{lang=deu}   
