[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/33.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=33

***

`Or`   
`10`   
`20`   
`25`   
`30`   
`95`   
`Urfundoen zum Jahre 1420.`{lang=deu} `21`   
`vertrunken 9 gr. — Heincze Bresens bote 2 gr. — Fischel kein`{lang=deu}   
`Bresslaw zu Jocoff Bawmgarten von Breslaw durch der lande`{lang=deu}   
`unde stete heimelicher botschaft wille 12 gr. — Hasse den diner`{lang=deu}   
`kein Budessin mit der ausschrift unseres herren des konigis, die`{lang=deu}   
`her von Bressla brochte, 7 gr.!)`{lang=deu}   
`In vigilia oculi`{lang=lat} `[Márz 9]:`{lang=deu}   
`Abir ein boten kein der Swidenicz unde vorbas kein Bress-`{lang=deu}   
`law zu unserm heutmanne durch des heutmannes willes von`{lang=deu}   
`der Swydeniez unde der Birsecke unde Nicl Kundelers wille`{lang=deu}   
`13 gr.?). — Mathis Kezer, der statschreiber mit den steten kein`{lang=deu}   
`der Lobaw zu tage durch unsers herren des koniges herfarte ge-`{lang=deu}   
`schefte wille unde des bisschoffs von Meissen unde der Welem-`{lang=deu}   
`schen wille mit den andern sachen !/» sch. — Nuncium ad Lu-`{lang=deu}   
`banum pro eadem causa 2 gr. — [Bl. 1557 Hannus von Muska`{lang=deu}   
`mit seinen frunden hotte einen tag vor den lantluten unde vor`{lang=deu}   
`deme rothe, worden geert unde vertrunken 9 gr. — Noch zweien`{lang=deu}   
`heimelichen orteil kein Meydeburg 417 fert.`{lang=deu}   
`In vigilia letare`{lang=lat} `[Mirz 16]:`{lang=deu}   
`Heinrich Numan, der statschreiber kein der Lobaw mit`{lang=deu}   
`landen unde steten zu tage, als unser herre der konig den landen`{lang=deu}   
`geschrebin hotte, das sie ein iderman off sein salde, kein Behem`{lang=deu}   
`zu zihen, unde sust von unsers herren brife wegen Deynhords`{lang=deu}   
`von Panewicz, Sigemund von Nethen und Heinrich Keias unde`{lang=deu}   
`des richters von Richenaw wegen etc. 32 gr. — Nuncium [ad]`{lang=deu}   
`Luban 2 gr. — Einen boten kein der Zitaw, das wir des tages`{lang=deu}   
`of den nehsten sonobend nicht gewarten konden, 3 gr. — /BI. 16 a]`{lang=deu}   
`Der herzoge von Berge zoch hie dorch kein Breslaw zu unseru .`{lang=deu}   
`herren, wort geert mit wyne 12 gr., item wort her geleitet mit`{lang=deu}   
`vir schutczen kein deme Bunczlaw 6 gr. — Item einen boten zu`{lang=deu}   
`den unsern keigen Breslaw zu dirfaren unsers herren des koniges`{lang=deu}   
`ernste meynunge umme den zog kein Behmen unde zu Gherhord`{lang=deu}   
`von der Hosen von ern Albrechtes wegen von Haulezendorff, also`{lang=deu}   
`ir beider geschefte ein ende gewon 19 gr. Umme die sachen`{lang=deu}   
`wort mit ern Albrechts frunden unde mit den mannen off deme`{lang=deu}   
`rothus vortrunken 8 gr. — Den buchsenmeistern vor 9 sch. schefte`{lang=deu}   
`À Dielleicht 8 gr., die Zahl tft verwifcht.`{lang=deu}   
`2) f. S. 20, 28 und Anm. 3.`{lang=deu}   
