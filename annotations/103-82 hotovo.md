[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/82.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=82

***

`70` ` Urkunden zum Jahre 1421.`{lang=deu}   
`In derselben zeit sein zu uns kommen in das veld der bischoff`{lang=deu}   
`von der Neyssen, herzog Cantener, der herzog von Monstirberg,`{lang=deu}   
`der von Colditz und Hanns von Polentz mit aller irer macht;`{lang=deu}   
`wy starg dy sein, das kónnen wir euch nicht eyentlich vor-`{lang=deu}   
`? schreiben, sundir wir mógin nicht dirkennen, das irne ”) ein`{lang=deu}   
`hauffe sterker sey denn der unser, und unser herre herzog`{lang=deu}   
`Rumpold, unser voyt, und wir werden haben den vorzog ubir`{lang=deu}   
`das gebirge vorbas uff dy ketzer, als wir nicht anders wissen,`{lang=deu}   
`morne des nehsten tages in sulcher weyse, als an uns kommen`{lang=deu}   
`10 ist, das der bischoff von Olmuntz etzwen zum Lewtemyschel`{lang=deu}   
`unde der Pypo brengen 24 thusunnt pferde unde werden sehen`{lang=deu}   
`uff uns unde wir uff sy. Aber nicht werden wir seyn mittenander`{lang=deu}   
`in eyme haufen in dem velde, sunder 4 meylin von enander,.`{lang=deu}   
`das eyn teil dem andern, als is not wurde sein, zu hülffe mochte`{lang=deu}   
`15 kommen etc. |`{lang=deu}   
`, Hirumme, liben herren, wir ouch itzund vormals zwir ge-`{lang=deu}   
`schrebin han umme zerunge, ist uns von euch nicht antwert`{lang=deu}   
`worden etc. Beten wir euch noch mit sunderlichem fleysse, das`{lang=deu}   
`ir doran gedenket unde das wellet ansehen unde dirkennen, das`{lang=deu}   
`^? wir eynen grossen hauffen haben unde grosse zerunge tragen,`{lang=deu}   
`unde uns sendet zerunge, der wir gros notdürfftig sein, itzund`{lang=deu}   
`sunderlich ubir das gebirge in der vinde land etc. Wenn`{lang=deu}   
`herzog Lodewig unde herzog Johann von Sagan nicht kommen`{lang=deu}   
`sein, wens?) ir uns senden welt, das moget ir mit en lossen`{lang=deu}   
`5 durchgehen eto.`{lang=deu}   
`Ouch als wir euch geschrebin haben, das wir haben uff-`{lang=deu}   
`genommen von Petir Hulferich 10 marg und von Hannus`{lang=deu}   
`Bewenitz?) von der Sweydenitz 27 marg groschen, das richtet en`{lang=deu}   
`zu dancke, wenn wir haben sein?) wedir groschen noch heller.`{lang=deu}   
`30 Ouch, liben herren, habet doruff gedancken uff dy losunge,`{lang=deu}   
`wenn die vier wochen ausskommen, das ir den gesellen losunge`{lang=deu}   
`sendet; sy sprechen, sy wellen losunge haben etc., nemen grossen`{lang=deu}   
`schaden an dem eren do heyme. Ouch, liben herren, bethe ich`{lang=deu}   
`euch als von mir, Herman Schultes, das ir einen andern schaffet,`{lang=deu}   
`i) für iren — eorum, i. Weinhold mittelhochdeutfch. Gramm, 1885. S. 524.`{lang=deu}   
`2) Griinhagen und Dalady [efen unrichtig „wir“, Der Sinn НЕ: Фа Berzog`{lang=deu}   
`£udwig und Herzog Johann noch nicht zu dem Heere gekommen find, fo fenbet die`{lang=deu}   
`Sebrung mit threm Fuge ju uns.`{lang=deu}   
`3) f. oben S. c2, 18.`{lang=deu}   
`4) Bier liegt woh! ein Sefefebler ober eine {törende Auslaffung des Scultet. vor.`{lang=deu}   
