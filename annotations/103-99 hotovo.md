[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/103/99.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=103&page=99

***

`RE O`{.error}   
`Urfunden zum Jahre 1422. `{lang=deu}`87`   
`von Meissen mit andern sachen etc. 8 fert. — Einen boten kein`{lang=deu}   
`Lemberg durch der Behmischen herren wille, die do hin komen`{lang=deu}   
`wolden, 4 gr.`{lang=deu}` |`{.error}   
`/[Dl. 114a]`{lang=deu}` Sabbato ante Johannis Baptiste`{lang=lat}` [Juni 20]:`{lang=deu}   
`Herman Schultes, der statschreiber mit landen unde steten`{lang=deu}   
`kein Lemberg zu tage mit den Polnischen fursten unde Behmi-`{lang=deu}   
`schen herren etc. 11/2 sch. — Einen boten zu Heinrich herzoge`{lang=deu}   
`deme eldern mit lande unde stete brife 8 gr. — Die rotmanne`{lang=deu}   
`von der Stregen worden!) geert mit wyne unde bire 8 gr. —`{lang=deu}   
`Land unde stete worden geert mit wyne unde bire, also sie kein`{lang=deu}   
`Lemberg zogen, 82 gr. — Meistir Syfrid, unsers herren des`{lang=deu}   
`koniges arczt, wart geert mit wyne unde bire 11 gr. —`{lang=deu}   
`/Bl. 114 6] Frenczil Silberman vor habir fure kein der Zitaw`{lang=deu}   
`23 gr. — Vor 200 cigels 8 gr. deme buchsenmeister zwischen`{lang=deu}   
`15 die mauer.`{lang=deu} ` |`{.error}   
`/Bl. 11507`{lang=deu} ` Sabbato ante Petri et Pauli`{lang=lat} ` [Juni 27]:`{lang=deu}   
`Herman Schultes, der statschreiber mit landen unde steten`{lang=deu}   
`kein der Lobaw zu tage durch der Misner wille unde der`{lang=deu}   
`Hussen ete. 28 gr. — `{lang=deu} `Nuncium ad Lubanum`{lang=lat} ` 2 gr. — Herman`{lang=deu}   
`? stellenmecher vor zweie buchsen rade 8 gr. — Vor acht bandt`{lang=deu}   
`zu deme bauhe bie deme stein torme 16 gr.; vor hócken do-`{lang=deu}   
`selbist 8 gr. — /Bl. 115^] Jorge Canicz vor ein kolner?), das`{lang=deu}   
`her zu Dresdan verloren had, 38 gr.`{lang=deu}   
`or`{.error}   
`10`   
`[Bl. 116a]`{lang=deu}` Sabbato in die saneti Procopii`{lang=lat}` [Juli 4]:`{lang=deu} ` |`{.error}   
`25 Mathis Kezer, Petir Bartholomeus mit landen unde steten`{lang=deu}   
`kein der Lobaw zu tage durch Petir Spengehoultzen wille mit`{lang=deu}   
`den von der Zitaw mit andern sachen etc. 29 gr. — Nuncium`{lang=deu}   
`ad Lubanum 2 gr. — Vor latten zu deme bliden hause 10 gr:`{lang=deu}   
`— Herman Schultes, Wenezla Monch mit landen unde. steten`{lang=deu}   
`50 kein Camencz zu tage mit Hannus Polenoz unde den steten von`{lang=deu}   
`Lusicz unde mit den rethen von Meissen mit vil sachen 21/» sch.`{lang=deu}   
`4 gr. — /Bl. 1165] Pate Hannus kein Budessein mit des foites`{lang=deu}   
`brife?), was ussen obir nacht durch sachen willen 8 gr. —`{lang=deu}   
`1) In der Dorlage wart.`{lang=deu}   
`?) śweifelsohne == Koller, wem id aud) fonft bafür kolner nicht nach:`{lang=deu}   
`weifen fann.`{lang=deu}   
`3) Es ift wohl der Brief des Herzogs Rumpold vom 23. Juni 1422 gemeint,`{lang=deu}   
`der unten abgedruckt ift.`{lang=deu}   
