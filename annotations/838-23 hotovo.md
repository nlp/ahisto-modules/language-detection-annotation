[![page image][image]][portal]

[image]: https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/838/23.jpg
[portal]: https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book=838&page=23

***

`8 Desky dvorské král. Českého:`{lang=ces}   
`Juxta : Inposieio a. d. MCCCCXVIII sabbato ante festum`{lang=lat}   
`S. Philippi et Jacobi apostolorum`{lang=lat} `[30. dubna].`{lang=ces} `Prevencio.`{lang=lat}   
`Juxta: Executor Henrieus de Elsterberg vel unus alius. Terminus sabbato ante`{lang=lat}   
`festum s. Viti`{lang=lat} `(17. cervna].`{lang=ces} `ll terminus sabbato ante festum s. Jacobi`{lang=lat} `[23. července).`{lang=ces}   
`Executor .“`{lang=lat}   
`©) Jméno nebylo připsáno.`{lang=ces}   
`26. Albertus de Coldiez* conqueritur super Vitum* de Schonburg et de Zyberg, de`{lang=lat}   
`Gluchow et de Waldenberg,” quia® fecit sibi dampnum suo posse sine iure in eius`{lang=lat}   
`hereditate omagiali in Bielyna et in Meru cum suis pertinenciis sibi per dominum`{lang=lat}   
`regem data et in Zybergk, et hoc dampnum in diversis impensis, in diversis domus`{lang=lat}   
`rebus” et in promptis peccuniis pro tribus milibus? marcis argenti. Post hoc dampnum`{lang=lat}   
`póvod sám vel Hannusius dictus Capplerz de Suleyowicz.?`{lang=lat}   
`Juxta: Inposicio a. d. MCCCCXVIII feria V post festum`{lang=lat}   
`s. Jacobi apostoli`{lang=lat} `[28. cervence]`{lang=ces} `pro dampnis MMM*'s`{lang=lat}   
`marcarum argenti.`{lang=lat}   
`Juxta: Executor nobilis Nicolaus de Hazemburg et de Budyna vel unus alius.`{lang=lat}   
`Terminus in vigilia s. Bartholomei`{lang=lat} `[23. srpna]`{lang=ces} `Ibi Vitus^ citatus infirmus; Wen-`{lang=lat}   
`ceslaus de Dubycze ponit. Terminus iurare^ pro infirmitate in crastino s. Jeronimi`{lang=lat}   
`[1. 7ájna].`{lang=ces} `Ibi Vitus secundo infirmus; Fridericus purgravius de Zyberg ponit. Terminus`{lang=lat}   
`iurare pro infirmitate in crastino s. Martini`{lang=lat} `[12. lstopadu].`{lang=ces} `Ibi Vitus citatus, positus`{lang=lat}   
`bis infirmus, ter vocatus, ad iurandum pro infirmitate non astitit. Ob hoc datum est`{lang=lat}   
`Alberto predicto actori pro iure obtento. Super hoc predictus Albertus dedit memo-`{lang=lat}   
`riales. Relacio omnium beneficiariorum. Actum a. d. MCCCCXVIIH die qua supra`{lang=lat}   
`[12. listopadu].`{lang=ces}   
`Data est monicio quatuordecim dierum.`{lang=lat}   
`Anno domini millesimo quadringentesimo decimo nono feria secunda post Cir-`{lang=lat}   
`cumcisionem Domini`{lang=lat} `[2. ledna]`{lang=ces} `predictus dominus Albertus actor per Martinum no-`{lang=lat}   
`tarium tabularum curie regalis ex parte tocius beneficii cum Tyczone dicto Móglin`{lang=lat}   
`de Luzicz, omagiali regio, super bona et hereditates omagiales” in Meru opido cum`{lang=lat}   
`agris, pratis, rivis, silvis, tabernis, cum censu et omni libertate ad ea pertinente no-`{lang=lat}   
`bilis Viti de Schonburgk predicti citati, quantum ibi habet tantum, inductus est iure`{lang=lat}   
`omagiali pleno iure, adhibitis ad hoc insigniis terra effossa et signo exciso de porta`{lang=lat}   
`civitatis predicte, in prefatà summa peccunie obtenta, et beneficium in tanta summa.`{lang=lat}   
`Et iste inductus notificatus est per litteram civitatibus in Czwiekaw et in Aldenbergk`{lang=lat}   
`et omnibus consulibus, iudiei ac toti communitati prefate civitatis in Meru. Actum`{lang=lat}   
`anno et die quibus supra`{lang=lat} `[2. ledna 1419].`{lang=ces}   
`Item a. d. MCCCCXIX feria V post Sigismundi`{lang=lat} `[4. kvéina]`{lang=ces} `data est littera mo-`{lang=lat}   
`nicionis denuo quatuordecim dierum contra Vitum citatum.`{lang=lat}   
`(*) Albertus committit Nicolao de castro Pragensi super lucro et dampno.*`{lang=lat}   
