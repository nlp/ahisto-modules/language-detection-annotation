# -*- coding:utf-8 -*

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger('create_annotated_hocr')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from pathlib import Path
from .common import gather_files, convert_markdown_to_hocr_with_template, is_markdown_fully_annotated


def main(output_path: Path = Path('output')) -> None:
    output_path.mkdir(exist_ok=True)
    files = gather_files()
    num_fully_annotated = 0

    for key, (annotation, template) in files.items():
        output = (output_path / key).with_suffix('.hocr')
        with annotation.open('rb') as af, template.open('rb') as tf:
            if not is_markdown_fully_annotated(af):
                continue
            num_fully_annotated += 1
            af.seek(0)
            rf = convert_markdown_to_hocr_with_template(af, tf)
            with output.open('wb') as wf:
                wf.write(rf.read())

    LOGGER.info(f'Converted {num_fully_annotated} finished annotations to HOCR out of {len(files)}')


if __name__ == '__main__':
    main()
