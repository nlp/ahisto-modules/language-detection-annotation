# -*- coding:utf-8 -*-

from io import BytesIO
from itertools import zip_longest
from pathlib import Path
import re
from subprocess import Popen, PIPE
from typing import Iterable, Tuple, BinaryIO, Optional, List, Dict, Any, SupportsFloat

from lxml import etree
import pycountry


Language = str


def stematize_filename(filename: Path) -> str:
    match = re.match(r'[0-9]+-[0-9]+', filename.stem)
    if match is None:
        raise ValueError(f'Malformed filename "{filename}"')
    stem = match.group()
    return stem


def normalize_language_code(language_code: str) -> Optional[Language]:
    try:
        language = pycountry.languages.lookup(language_code)
    except LookupError:
        return None
    return language.alpha_3


def l1_normalize(dictionary: Dict[Any, SupportsFloat]) -> Dict[Any, float]:
    value_sum = sum(map(float, dictionary.values()))
    if value_sum == 0.0:
        value_sum = 1.0
    return {
        key: float(value) / value_sum
        for key, value
        in dictionary.items()
        if value > 0.0
    }


def normalize_word(word: str) -> str:
    return word.strip()


Document = etree.ElementTree


def dump_html(document: Document) -> BinaryIO:
    wf = BytesIO()
    document.write(wf, method='html', xml_declaration=True)
    wf.seek(0)
    return wf


def parse_html(f: BinaryIO) -> Document:
    html_parser = etree.HTMLParser(huge_tree=True, encoding='utf-8')
    document = etree.parse(f, html_parser)
    return document


Word = etree._Element
Line = Tuple[Word]


def iterate_html_lines(document: Document) -> Iterable[Line]:
    for paragraph in document.xpath('//p[@class="ocr_par"]'):
        for line in paragraph.xpath('span[@class="ocr_line"]'):
            line = (word for word in line.xpath('span[@class="ocrx_word"]'))
            line = filter(lambda word: word.text is not None and normalize_word(word.text), line)
            line = tuple(line)
            if line:
                yield line


def iterate_hocr_lines(f: BinaryIO) -> Iterable[Line]:
    for line in iterate_html_lines(parse_html(f)):
        yield line


def iterate_markdown_lines(f: BinaryIO) -> Iterable[Line]:
    for line in iterate_hocr_lines(convert_markdown_to_hocr(f)):
        yield line


def is_markdown_fully_annotated(f: BinaryIO) -> bool:
    for line in iterate_markdown_lines(f):
        for word in line:
            if 'unchecked' in word.attrib:
                return False
    return True


def convert_markdown_to_hocr_with_template(af: BinaryIO, tf: BinaryIO) -> BinaryIO:
    document = parse_html(tf)

    for element in document.xpath('//*[@lang]'):
        del element.attrib['lang']

    lines = zip_longest(iterate_markdown_lines(af), iterate_html_lines(document), fillvalue=None)
    for annotation_line, template_line in lines:
        assert annotation_line is not None
        assert template_line is not None
        words = zip_longest(annotation_line, template_line, fillvalue=None)
        for annotation_word, template_word in words:
            assert annotation_word is not None
            assert template_word is not None
            assert normalize_word(annotation_word.text) == normalize_word(template_word.text)
            if 'lang' in annotation_word.attrib:
                template_word.attrib['lang'] = annotation_word.attrib['lang']

    return dump_html(document)


def convert_markdown_to_hocr(f: BinaryIO) -> BinaryIO:
    return convert_html_to_hocr(convert_markdown_to_html(f))


def convert_markdown_to_html(f: BinaryIO) -> BinaryIO:
    args = ['pandoc', '-s', '-f', 'markdown+inline_code_attributes+hard_line_breaks', '-t', 'html']
    process = Popen(args, stdin=f, stdout=PIPE, stderr=PIPE)
    html_output, stderr = process.communicate()
    if process.returncode != 0:
        raise ValueError(stderr)
    assert html_output is not None
    return BytesIO(html_output)


def convert_html_to_hocr(rf: BinaryIO) -> BinaryIO:
    html = etree.Element('html')
    body = etree.SubElement(html, 'body')
    page = etree.SubElement(body, 'div', {'class': 'ocr_page'})
    paragraph = etree.SubElement(page, 'p', {'class': 'ocr_par'})

    unfinished_line: List[str] = []
    unfinished_attributes: List[Dict[str, str]] = []
    extra_text: Optional[str] = None

    def normalize_attributes(attributes: Dict[str, str]) -> Dict[str, str]:
        normalized_attributes: Dict[str, str] = dict()
        for key, value in attributes.items():
            if key not in ('class', 'lang'):
                raise ValueError(f'Unknown attribute "{key}={value}" in line "{get_current_line()}"')
            if key == 'class':
                classes = set(value.split())
                extra_classes = classes - {'unchecked', 'error'}
                if extra_classes:
                    extra_classes_string = ' '.join(f'.{_class}' for _class in sorted(classes))
                    raise ValueError(f'Unknown classes "{extra_classes_string}" in line "{get_current_line()}"')
                for _class in classes:
                    normalized_attributes[_class] = _class
            if key == 'lang':
                normalized_value = normalize_language_code(value)
                if normalized_value is None:
                    raise ValueError(f'Unknown language code "{value}" in line "{get_current_line()}"')
                normalized_attributes[key] = normalized_value
        return normalized_attributes

    def append_to_line(element: etree._Element) -> None:
        nonlocal extra_text
        if element.text is not None:
            if element.tail is not None and normalize_word(element.tail):
                text = f'{element.text} {normalize_word(element.tail)}'
            else:
                text = element.text
            unfinished_line.append(text)
            unfinished_attributes.append(normalize_attributes(element.attrib))
        if extra_text is None and element.tail is not None and normalize_word(element.tail):
            extra_text = normalize_word(element.tail)

    def get_current_line(is_finished: bool = False) -> str:
        line = ' '.join(' '.join(unfinished_line).split())
        return line if is_finished else f'{line} ...'

    def finish_line() -> None:
        nonlocal extra_text
        assert len(unfinished_line) == len(unfinished_attributes)
        if extra_text is not None:
            raise ValueError(f'Extra untagged text "{extra_text}" in line "{get_current_line()}"')
        if not unfinished_line:
            return
        line = etree.SubElement(paragraph, 'span', {'class': 'ocr_line'})
        for text, attributes in zip(unfinished_line, unfinished_attributes):
            for word in text.split():
                subelement = etree.SubElement(line, 'span', {'class': 'ocrx_word', **attributes})
                subelement.text = word
                subelement.tail = ' '
            subelement.tail = ''
        unfinished_line.clear()
        unfinished_attributes.clear()
        extra_text = None

    for element in parse_html(rf).xpath('//hr/following::code | //hr/following::br'):
        if element.tag == 'br':
            finish_line()
        append_to_line(element)
    finish_line()

    document = etree.ElementTree(html)
    return dump_html(document)


def gather_files(annotation_path: Path = Path('annotations'),
                 template_path: Path = Path('templates')) -> Dict[str, Tuple[Path, Path]]:
    annotations = {stematize_filename(filename): filename for filename in annotation_path.glob('**/*.md')}
    templates = {stematize_filename(filename): filename for filename in template_path.glob('**/*.hocr')}

    if annotations.keys() - templates.keys():
        missing_keys = annotations.keys() - templates.keys()
        message = f'HOCR templates {missing_keys} are missing from {template_path}/*.hocr'
        raise ValueError(message)

    if templates.keys() - annotations.keys():
        missing_keys = templates.keys() - annotations.keys()
        message = f'Markdown annotations {missing_keys} are missing from {annotation_path}/*.md'
        raise ValueError(message)

    return {
        key: (annotations[key], templates[key])
        for key
        in annotations.keys()
    }
