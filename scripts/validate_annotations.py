# -*- coding:utf-8 -*

import logging


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger('validate_annotations')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format=LOGGING_FORMAT)


from collections import Counter
from itertools import zip_longest
from multiprocessing import Pool
from pathlib import Path
from typing import Tuple, Optional
import sys

from .common import iterate_hocr_lines, iterate_markdown_lines, l1_normalize, gather_files, normalize_word


def validate_one(annotation: Path, template: Path) -> Tuple[int, int, Counter]:
    checked_words, total_words, languages = 0, 0, Counter()
    with annotation.open('rb') as af, template.open('rb') as tf:
        lines = zip_longest(iterate_markdown_lines(af), iterate_hocr_lines(tf), fillvalue=None)
        for line_number, (annotation_line, template_line) in enumerate(lines):
            if annotation_line is None:
                message = f'HOCR template contains more lines than markdown annotations ({line_number})'
                raise ValueError(message)

            if template_line is None:
                message = f'Markdown annotations contain more lines than HOCR template ({line_number})'
                raise ValueError(message)

            for word in annotation_line:
                total_words += 1
                if 'unchecked' not in word.attrib:
                    checked_words += 1
                    if 'lang' in word.attrib:
                        languages[word.attrib['lang']] += 1
                    else:
                        languages[None] += 1

            annotation_line_tuple = tuple(normalize_word(word.text) for word in annotation_line)
            template_line_tuple = tuple(normalize_word(word.text) for word in template_line)

            if annotation_line_tuple != template_line_tuple:
                message = 'On line {}, markdown words {} differ from HOCR words {}'
                message = message.format(line_number + 1, annotation_line_tuple, template_line_tuple)
                raise ValueError(message)
    return checked_words, total_words, languages


def validate_one_helper(args: Tuple[str, Tuple[Path, Path]]) -> Optional[Tuple[int, int, Counter]]:
    key, (annotation, template) = args
    try:
        return validate_one(annotation, template)
    except ValueError as e:
        LOGGER.error(f'{key}: {e}')
        return None


def validate() -> Tuple[int, int, Counter]:
    checked_words, total_words, languages = 0, 0, Counter()
    with Pool(None) as pool:
        files = gather_files().items()
        for stats in pool.imap_unordered(validate_one_helper, files):
            if stats is None:
                sys.exit(1)
            checked_words_one, total_words_one, languages_one = stats
            checked_words += checked_words_one
            total_words += total_words_one
            languages += languages_one
    return checked_words, total_words, languages


def main() -> None:
    checked_words, total_words, languages = validate()

    LOGGER.info(f'{checked_words * 100.0 / total_words:.02f}% of the dataset has already been annotated')

    LOGGER.info('Language statistics:')
    for key, value in sorted(l1_normalize(languages).items(), key=lambda x: (-x[1], x[0])):
        LOGGER.info(f'* {key}: {value * 100.0:.02f}%')


if __name__ == '__main__':
    main()
